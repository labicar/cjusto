<?php
// Heading
$_['heading_title'] = 'Análisis de ventas';

// Text
$_['text_order']    = 'Pedidos';
$_['text_customer'] = 'Clientes';
$_['text_day']      = 'Hoy';
$_['text_week']     = 'Semanal';
$_['text_month']    = 'Mensual';
$_['text_year']     = 'Anual';