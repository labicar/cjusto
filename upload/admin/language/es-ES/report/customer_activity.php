<?php
// Heading
$_['heading_title']       = 'Reporte de actividades del cliente';

// Text
$_['text_list']           = 'Lista de actividades del cliente';
$_['text_address_add']    = '<a href="customer_id=%d"> %s</a> agregó una nueva dirección.';
$_['text_address_edit']   = '<a href="customer_id=%d"> %s</a> ha actualizado su dirección.';
$_['text_address_delete'] = '<a href="customer_id=%d"> %s</a> ha borrado una de sus direcciones.';
$_['text_edit']           = '<a href="customer_id=%d"> %s</a> ha actualizado la información de su cuenta.';
$_['text_forgotten']      = '<a href="customer_id=%d"> %s</a> ha solicitado una nueva contraseña.';
$_['text_login']          = '<a href="customer_id=%d"> %s</a> se ha conectado.';
$_['text_password']       = '<a href="customer_id=%d"> %s</a> ha actualizado su contraseña.';
$_['text_register']       = '<a href="customer_id=%d">%s</a> registró una cuenta.';
$_['text_return_account'] = '<a href="customer_id=%d"> %s</a> presentó una devolución de producto.';
$_['text_return_guest']   = '%s presentó una devolución de producto.';
$_['text_order_account']  = '<a href="customer_id=%d">%s</a> creó una <a href="order_id=%d"> nueva orden</a>.';
$_['text_order_guest']    = '%s creó un el pedido <a href="order_id=%d"></a>.';

// Column
$_['column_customer']     = 'Cliente';
$_['column_comment']      = 'Comentario';
$_['column_ip']           = 'IP';
$_['column_date_added']   = 'Agregar Fecha';

// Entry
$_['entry_customer']      = 'Cliente';
$_['entry_ip']            = 'IP';
$_['entry_date_start']    = 'Fecha de Inicio';
$_['entry_date_end']      = 'Fecha Final';