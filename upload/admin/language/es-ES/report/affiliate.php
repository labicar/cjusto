<?php
// Heading
$_['heading_title']     = 'Informe de las Comisiones de afiliados';

// Text
$_['text_list']         = 'Lista de las comisiones de afiliados';

// Column
$_['column_affiliate']  = 'Nombre de afiliado';
$_['column_email']      = 'E-Mail';
$_['column_status']     = 'Estado';
$_['column_commission'] = 'Comisión';
$_['column_orders']     = 'Num. Pedidos';
$_['column_total']      = 'Total ';
$_['column_action']     = 'Acción';

// Entry
$_['entry_date_start']  = 'Fecha de Inicio';
$_['entry_date_end']    = 'Fecha Final';