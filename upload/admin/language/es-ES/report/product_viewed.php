<?php
// Heading
$_['heading_title']    = 'Repoprte de productos vistos';

// Text
$_['text_list']        = 'Listado de productos vistos';
$_['text_success']     = 'Éxito: Se ha reiniciado el informe de productos vistos!';

// Column
$_['column_name']      = 'Nombre del Producto';
$_['column_model']     = 'Modelo';
$_['column_viewed']    = 'Visto';
$_['column_percent']   = 'Porcentaje';

// Error
$_['error_permission'] = 'ADVERTENCIA: No tienes permiso para reiniciar producto el reporte de productos vistos!';