<?php
// Heading
$_['heading_title']    = 'Informe de cupones';

// Text
$_['text_list']        = 'Listado de cupones';

// Column
$_['column_name']      = 'Nombre de cupón';
$_['column_code']      = 'Código';
$_['column_orders']    = 'Pedidos';
$_['column_total']     = 'Total ';
$_['column_action']    = 'Acción';

// Entry
$_['entry_date_start'] = 'Fecha de Inicio';
$_['entry_date_end']   = 'Fecha Final';