<?php
// Heading
$_['heading_title']             = 'Afiliados';

// Text
$_['text_success']              = 'Éxito: Ha modificado los afiliados!';
$_['text_approved']             = 'Usted ha aprobado %s! cuentas!';
$_['text_list']                 = 'Lista de afiliados';
$_['text_add']                  = 'Añadir afiliado';
$_['text_edit']                 = 'Editar afiliado';
$_['text_balance']              = 'Saldo';
$_['text_cheque']               = 'Cheque';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Transferencia bancaria';

// Column
$_['column_name']               = 'Nombre de afiliado';
$_['column_email']              = 'E-Mail';
$_['column_code']               = 'Código de seguimiento';
$_['column_balance']            = 'Saldo';
$_['column_status']             = 'Estado';
$_['column_approved']           = 'Aprobado';
$_['column_date_added']         = 'Agregar Fecha';
$_['column_description']        = 'Descripción';
$_['column_amount']             = 'Importe';
$_['column_action']             = 'Acción';

// Entry
$_['entry_firstname']           = 'Nombre';
$_['entry_lastname']            = 'Apellidos';
$_['entry_email']               = 'E-Mail';
$_['entry_telephone']           = 'Teléfono';
$_['entry_fax']                 = 'Fax';
$_['entry_status']              = 'Estado';
$_['entry_password']            = 'Contraseña';
$_['entry_confirm']             = 'Confirmar';
$_['entry_company']             = 'Empresa';
$_['entry_website']             = 'Sitio Web';
$_['entry_address_1']           = 'Dirección 1';
$_['entry_address_2']           = 'Dirección 2';
$_['entry_city']                = 'Ciudad';
$_['entry_postcode']            = 'Código postal';
$_['entry_country']             = 'País';
$_['entry_zone']                = 'Región / Provincia';
$_['entry_code']                = 'Código de seguimiento';
$_['entry_commission']          = 'Comisión (%)';
$_['entry_tax']                 = 'NIF (Número de Identificación Fiscal)';
$_['entry_payment']             = 'Modalidad de Pago';
$_['entry_cheque']              = 'Nombre del beneficiario cheque';
$_['entry_paypal']              = 'Cuenta de correo electrónico de PayPal';
$_['entry_bank_name']           = 'Nombre del Banco';
$_['entry_bank_branch_number']  = 'ABA/BSB (número de sucursal)';
$_['entry_bank_swift_code']     = 'Código SWIFT';
$_['entry_bank_account_name']   = 'Nombre de cuenta';
$_['entry_bank_account_number'] = 'Número de cuenta';
$_['entry_amount']              = 'Importe';
$_['entry_description']         = 'Descripción';
$_['entry_name']                = 'Nombre de afiliado';
$_['entry_approved']            = 'Aprobado';
$_['entry_date_added']          = 'Agregar Fecha';

// Help
$_['help_code']                 = 'El código de seguimiento que se utilizará para rastrear las referencias.';
$_['help_commission']           = 'Porcentaje que el afiliado recibe en cada pedido.';

// Error
$_['error_permission']          = 'Advertencia: No tienes permiso para modificar los afiliados!';
$_['error_exists']              = 'ADVERTENCIA: Dirección de correo electrónico ya está registrada!';
$_['error_firstname']           = 'El nombre debe tener entre 1 y 21 caracteres!';
$_['error_lastname']            = 'El apellido debe tener entre 1 y 21 caracteres!';
$_['error_email']               = 'La dirección de correo electrónico no parece ser válida!';
$_['error_cheque']              = 'Nombre del beneficiario del cheque necesario!';
$_['error_paypal']              = 'La dirección de correo electrónico de PayPal no parece ser válida!!';
$_['error_bank_account_name']   = 'Nombre de la cuenta requerido!';
$_['error_bank_account_number'] = 'Número de cuenta requerido!';
$_['error_telephone']           = '¡El teléfono debe tener entre 3 y 32 caracteres!';
$_['error_password']            = 'La Contraseña debe poseer entre 4 y 20 caracteres!';
$_['error_confirm']             = 'La contraseña y su confirmación no coinciden!';
$_['error_address_1']           = 'La dirección debe ser entre 3 y 128 caracteres!';
$_['error_city']                = 'La ciudad debe ser entre 2 y 128 caracteres!';
$_['error_postcode']            = 'El código postal debe ser entre 2 y 10 caracteres para este país!';
$_['error_country']             = 'Por favor seleccione un Pais!';
$_['error_zone']                = 'Por favor, seleccione una región / estado!';
$_['error_code']                = 'Código de seguimiento requerido!';