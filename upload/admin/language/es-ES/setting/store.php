<?php
// Heading
$_['heading_title']                    = 'Tiendas';

// Text
$_['text_settings']                    = 'Configuración';
$_['text_success']                     = 'Has modificado las tiendas correctamente.';
$_['text_list']                        = 'Lista de tiendas';
$_['text_add']                         = 'Añadir tienda';
$_['text_edit']                        = 'Editar tienda';
$_['text_items']                       = 'Artículos';
$_['text_tax']                         = 'Impuestos';
$_['text_account']                     = 'Cuenta';
$_['text_checkout']                    = 'Realizar pedido';
$_['text_stock']                       = 'Stock';
$_['text_shipping']                    = 'Dirección de envío';
$_['text_payment']                     = 'Dirección de pago';

// Column
$_['column_name']                      = 'Nombre de la Tienda';
$_['column_url']	                   = 'URL de la tienda';
$_['column_action']                    = 'Acción';

// Entry
$_['entry_url']                        = 'URL de la tienda';
$_['entry_ssl']                        = 'SSL URL';
$_['entry_name']                       = 'Nombre de la Tienda';
$_['entry_owner']                      = 'Dueño de la Tienda';
$_['entry_address']                    = 'Dirección';
$_['entry_geocode']                    = 'Geocode';
$_['entry_email']                      = 'E-Mail';
$_['entry_telephone']                  = 'Teléfono';
$_['entry_fax']                        = 'Fax';
$_['entry_image']                      = 'Imagen';
$_['entry_open']                       = 'Horario de Apertura';
$_['entry_comment']                    = 'Comentario';
$_['entry_location']                   = 'Localización de la Tienda';
$_['entry_meta_title']                 = 'Meta Título';
$_['entry_meta_description']           = 'Etiqueta de Meta Descripción';
$_['entry_meta_keyword']               = 'Etiqueta de Palabras Claves';
$_['entry_layout']                     = 'Disposición Predefinida';
$_['entry_template']                   = 'Plantilla';
$_['entry_country']                    = 'País';
$_['entry_zone']                       = 'Región / Provincia';
$_['entry_language']                   = 'Idioma';
$_['entry_currency']                   = 'Moneda';
$_['entry_product_limit'] 	           = 'Elementos predeterminados por página (Catálogo)';
$_['entry_product_description_length'] = 'Límite de la Lista de Descripción (Catálogo)';
$_['entry_tax']                        = 'Mostrar Precios con Impuestos';
$_['entry_tax_default']                = 'Usar la Dirección Fiscal de la Tienda';
$_['entry_tax_customer']               = 'Usar la Dirección de Impuesto del Cliente';
$_['entry_customer_group']             = 'Grupo de Clientes';
$_['entry_customer_group_display']     = 'Grupos de clientes';
$_['entry_customer_price']             = 'Mostrar Precios sólo a Clientes';
$_['entry_account']                    = 'Términos de la Cuenta';
$_['entry_cart_weight']                = 'Mostrar Peso en la Página del Carrito';
$_['entry_checkout_guest']             = 'Comprar como invitado';
$_['entry_checkout']                   = 'Términos de Pago';
$_['entry_order_status']               = 'Estado del pedido';
$_['entry_stock_display']              = 'Mostrar Stock';
$_['entry_stock_checkout']             = 'Verificación de Stock';
$_['entry_logo']                       = 'Logo de la Tienda';
$_['entry_icon']                       = 'Ícono';
$_['entry_image_category']             = 'Tamaño de la Imagen en la Categoría';
$_['entry_image_thumb']                = 'Tamaño de la Imagen Miniatura en los Productos';
$_['entry_image_popup']                = 'Tamaño de la Imagen en el Popup del Producto';
$_['entry_image_product']              = 'Tamaño de la Imagen en la Lista de Productos';
$_['entry_image_additional']           = 'Tamaño de la Imagen en el Producto Adicional';
$_['entry_image_related']              = 'Tamaño de la Imagen del Producto Relacionado';
$_['entry_image_compare']              = 'Tamaño de la Imagen al Comparar';
$_['entry_image_wishlist']             = 'Tamaño de la Imagen en la Lista de Deseos';
$_['entry_image_cart']                 = 'Tamaño de la imagen del carro';
$_['entry_image_location']             = 'Tamaño de la imagen de tienda';
$_['entry_width']                      = 'Ancho';
$_['entry_height']                     = 'Alto';
$_['entry_secure']                     = 'Usar SSL';

// Help
$_['help_url']                         = 'Incluir la URL completa a tu tienda. Asegúrate de añadir \'/\' al final. Ejemplo: http://www.tudominio.com/ruta/<br /><br />No uses directorios para crear nuevas tiendas. Deberías usar otros dominios o sub dominios apuntando a tu hosting.';
$_['help_ssl']                         = 'SSL URL de tu tienda. Asegúrate de añadir \'/\' al final. Ejemplo: http://www.tudominio.com/ruta/<br /><br />No uses directorios para crear nuevas tiendas. Deberías usar otros dominios o sub dominios apuntando a tu hosting.';
$_['help_geocode']                     = 'Introduce manual el GeoCoding de ubicación de la tienda.';
$_['help_open']                        = 'Indique su horario de apertura de las tiendas.';
$_['help_comment']                     = 'Este campo es para cualquier nota especial que te gustaría decirle al cliente. Por ejemplo: La tienda no acepta cheques.';
$_['help_location']                    = 'Las diferentes localizaciones de tiendas que tienes y que se mostrarán en el formulario de contacto.';
$_['help_currency']                    = 'Cambiar la moneda por defecto. Vaciar la caché del navegador para ver el cambio y reconfigurar su cookie existente.';
$_['help_product_limit'] 	           = 'Determina cuántos artículos del catálogo se muestran por página (productos, categorías, etc.)';
$_['help_product_description_length']  = 'En la vista de lista, limita el número de caracteres de la descripción (categorías, especial etc.)';
$_['help_tax_default']                 = 'Utilice la dirección de tienda para calcular los impuestos si nadie está conectado. Puede utilizar la dirección de la tienda como la dirección de envío o pago de clientes.';
$_['help_tax_customer']                = 'Use la dirección por defecto del cliente cuando este conectado para calcular los impuestos. Puede escoger usar la dirección por defecto para la dirección de envío o de pago del cliente.';
$_['help_customer_group']              = 'Grupo de clientes por defecto.';
$_['help_customer_group_display']      = 'Mostrar grupos de clientes que los nuevos clientes pueden seleccionar tales como mayoristas y negocios al registrarse.';
$_['help_customer_price']              = 'Mostrar precios sólo cuando un cliente haya iniciado sesión.';
$_['help_account']                     = 'Obligar a las personas aceptaro los términos del servicio antes de que pueda crearse una cuenta.';
$_['help_checkout_guest']              = 'Permiten a los clientes comprar sin crear una cuenta. Esto no estará disponible cuando exista un producto descargable en el carrito de compras.';
$_['help_checkout']                    = 'Obligar al cliente a aceptar los términos del servicio antes de pagar.';
$_['help_order_status']                = 'Establece el estado del pedido por defecto cuando se procesa un pedido.';
$_['help_stock_display']               = 'Mostrar cantidad de stock en la página del producto.';
$_['help_stock_checkout']              = 'Permitir a los clientes comprar el producto aunque no haya stock.';
$_['help_icon']                        = 'El icono debe tener formato PNG de 16px x 16px.';
$_['help_secure']                      = 'Para usar SSL verifica con tu proveedor de hospedaje para saber si hay un certificado SSL instalado.';

// Error
$_['error_warning']                    = '¡Advertencia: Por favor revise cuidadosamente los errores en el formulario!';
$_['error_permission']                 = 'No tienes permiso para modificar las tiendas.';
$_['error_name']                       = 'El nombre de la tienda debe tener entre 3 y 32 caracteres.';
$_['error_owner']                      = 'El nombre del propietario debe tener entre 3 y 64 caracteres.';
$_['error_address']                    = 'La dirección de la tienda debe tener entre 10 y 256 caracteres.';
$_['error_email']                      = 'La dirección de correo electrónico no parece ser válida!';
$_['error_telephone']                  = '¡El teléfono debe tener entre 3 y 32 caracteres!';
$_['error_url']                        = 'La URL de la tienda es obligatoria.';
$_['error_meta_title']                 = 'El título debe tener entre 3 y 32 caracteres.';
$_['error_limit']       	           = 'Límite requerido!';
$_['error_customer_group_display']     = 'Debe incluir el grupo de cliente por defecto si vas a utilizar esta función!';
$_['error_image_thumb']                = '¡Se requiere del tamaño de la imagen del producto!';
$_['error_image_popup']                = '¡Se requiere del tamaño de la ventana emergente para imagen del producto!';
$_['error_image_product']              = '¡Se requieren las dimensiones para el tamaño de la lista de producto!';
$_['error_image_category']             = '¡Se requieren las dimensiones para el tamaño de la lista de categorías!';
$_['error_image_additional']           = '¡Se requieren las dimensiones para el tamaño de la lista adicional de producto!';
$_['error_image_related']              = '¡Se requieren las dimensiones para el tamaño de la lista de productos relacionados!';
$_['error_image_compare']              = '¡Se requieren las dimensiones para el tamaño de la imagen comparada!';
$_['error_image_wishlist']             = '¡Se requieren las dimensiones para el tamaño de la lista de deseos!';
$_['error_image_cart']                 = 'El tamaño del la imagen del Carrito es requerido!';
$_['error_image_location']             = 'El tamaño de imagen de la tienda es requerido!';
$_['error_default']                    = 'ADVERTENCIA: No se puede borrar tu tienda por defecto!';
$_['error_store']                      = '¡Advertencia: Esta tienda no se puede eliminar ya que actualmente está asignada a %s ordenes!';