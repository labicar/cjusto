<?php
// Heading
$_['heading_title']     = 'Grupos de Atributos';

// Text
$_['text_success']      = '¡Has modificado exitosamente el grupo de atributos!';
$_['text_list']         = 'Lista de Grupos de Atributo';
$_['text_add']          = 'Agregar un Grupo de Atributos';
$_['text_edit']         = 'Editar un Grupo de Atributos';

// Column
$_['column_name']       = 'Nombre del Grupo de Atributo';
$_['column_sort_order'] = 'Ordenar por';
$_['column_action']     = 'Acción';

// Entry
$_['entry_name']        = 'Nombre del Grupo de Atributo';
$_['entry_sort_order']  = 'Ordenar por';

// Error
$_['error_permission']  = '¡Adveretencia: No tienes permiso para modificar el grupos de atributos!';
$_['error_name']        = '¡El nombre del Grupo de Atributos debe tener entre 3 y 64 carácteres!';
$_['error_attribute']   = '¡Advertencia: Este grupo de atributos no se puede borrar actualmente ya que está asignado a %s atributos!';
$_['error_product']     = '¡Advertencia: Este grupo de atributos no puede ser borrado ya que está asignado a %s productos!';