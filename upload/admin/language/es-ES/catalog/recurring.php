<?php
// Heading
$_['heading_title']			= 'Perfiles Recurrentes';

// Text
$_['text_success']          = '¡Has modificado correctamente los perfiles recurrentes!';
$_['text_list']             = 'Lista de Perfiles Recurrentes';
$_['text_add']              = 'Agregar Perfil Recurrente';
$_['text_edit']             = 'Editar Perfil Recurrente';
$_['text_day']				= 'Diario';
$_['text_week']				= 'Semanal';
$_['text_semi_month']		= 'Quincenal';
$_['text_month']			= 'Mensual';
$_['text_year']				= 'Anual';
$_['text_recurring']	    = '<p><i class="fa fa-info-circle"></i> Los montos recurrentes son calculados por la frecuencia y ciclos. </p><p>Por ejemplo si usa una frecuencia "semanal" y un ciclo de "2", entonces al usuario se le cobrará cada 2 semanas.</p><p>La duración es el número de veces que el usuario realiza los pagos, coloquelo en 0 si quieres que los pagos sean anulados.</p>';
$_['text_profile']			= 'Perfil Recurrente';
$_['text_trial']			= 'Perfil de Prueba';

// Entry
$_['entry_name']			= 'Nombre';
$_['entry_price']			= 'Precio';
$_['entry_duration']		= 'Duración';
$_['entry_cycle']			= 'Ciclo';
$_['entry_frequency']		= 'Frecuencia';
$_['entry_trial_price']		= 'Precio de Prueba';
$_['entry_trial_duration']	= 'Duración de la Prueba';
$_['entry_trial_status']	= 'Estatus de la Prueba';
$_['entry_trial_cycle']	    = 'Ciclo de Prueba';
$_['entry_trial_frequency']	= 'Frecuencia de la Prueba';
$_['entry_status']			= 'Estado';
$_['entry_sort_order']		= 'Ordenar por';

// Column
$_['column_name']			= 'Nombre';
$_['column_sort_order']	    = 'Ordenar por';
$_['column_action']         = 'Acción';

// Error
$_['error_warning']         = '¡Advertencia: Por favor revise cuidadosamente los errores en el formulario!';
$_['error_permission']		= '¡Advertencia: No tienes permiso para modificar perfiles recurrentes!';
$_['error_name']			= '¡El Nombre del producto debe ser mayor que 3 y menor que 255 caracteres!';
$_['error_product']			= '¡Advertencia: Este perfil recurrente no se puede eliminar porque actualmente se asigna a los productos %s!';