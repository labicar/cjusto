<?php
// Heading
$_['heading_title']          = 'Información';

// Text
$_['text_success']           = 'Información modificada exitosamente!';
$_['text_list']              = 'Lista de información';
$_['text_add']               = 'Agregar información';
$_['text_edit']              = 'Editar información';
$_['text_default']           = 'Predefinido';

// Column
$_['column_title']           = 'Información del Titulo';
$_['column_sort_order']	     = 'Ordenar por';
$_['column_action']          = 'Acción';

// Entry
$_['entry_title']            = 'Información del Titulo';
$_['entry_description']      = 'Descripción';
$_['entry_store']            = 'Tiendas';
$_['entry_meta_title'] 	     = 'Etiqueta de Meta Título';
$_['entry_meta_keyword'] 	 = 'Etiqueta de Palabras Claves';
$_['entry_meta_description'] = 'Etiqueta de Meta Descripción';
$_['entry_keyword']          = 'SEO Palabras Claves';
$_['entry_bottom']           = 'Parte inferior';
$_['entry_status']           = 'Estado';
$_['entry_sort_order']       = 'Ordenar por';
$_['entry_layout']           = 'Anulación de la disposición';

// Help
$_['help_keyword']           = 'No utilice espacios, en su lugar utilice el guión - y asegúrese que la palabra clave es única.';
$_['help_bottom']            = 'Mostrar en el pie inferior.';

// Error
$_['error_warning']          = '¡Advertencia: Por favor revise cuidadosamente los errores en el formulario!';
$_['error_permission']       = 'ADVERTENCIA: No tienes permiso para modificar la información.';
$_['error_title']            = 'El título debe contener entre 3 y 64 caracteres';
$_['error_description']      = 'La descripción debe tener más de 3 caracteres!';
$_['error_meta_title']       = '¡Meta Título debe ser mayor a 3 y menor a 255 caracteres!';
$_['error_keyword']          = '¡Palabra clave SEO ya en uso!';
$_['error_account']          = 'ADVERTENCIA: La información de esta página no puede ser suprimida porque actualmente está asignada como "términos de cuenta de la tienda"!';
$_['error_checkout']         = 'ADVERTENCIA: La información de esta página no puede ser suprimida porque actualmente está asignada como "términos de pago de la tienda"!';
$_['error_affiliate']        = 'ADVERTENCIA: La información de esta página no puede ser suprimida porque actualmente está asignada como "términos de afiliados de la tienda"!';
$_['error_return']           = 'ADVERTENCIA: La información de esta página no puede ser suprimida porque actualmente está asignada como "términos de devoluciones de la tienda"!';
$_['error_store']            = 'ADVERTENCIA: Esta página de la información no puede ser suprimida como su actualmente utilizado por %s tiendas!';