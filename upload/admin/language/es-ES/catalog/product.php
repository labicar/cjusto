<?php
// Heading
$_['heading_title']          = 'Productos';

// Text
$_['text_success']           = '¡Ha modificado correctamente los productos!';
$_['text_list']              = 'Lista de Productos';
$_['text_add']               = 'Agregar Producto';
$_['text_edit']              = 'Editar Producto';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Predefinido';
$_['text_option']            = 'Opción';
$_['text_option_value']      = 'Valor de la Opción';
$_['text_percent']           = 'Porcentaje';
$_['text_amount']            = 'Cantidad Fija';

// Column
$_['column_name']            = 'Nombre del Producto';
$_['column_model']           = 'Modelo';
$_['column_image']           = 'Imagen';
$_['column_price']           = 'Precio';
$_['column_quantity']        = 'Cantidad';
$_['column_status']          = 'Estado';
$_['column_action']          = 'Acción';

// Entry
$_['entry_name']             = 'Nombre del Producto';
$_['entry_description']      = 'Descripción';
$_['entry_meta_title'] 	     = 'Etiqueta de Meta Título';
$_['entry_meta_keyword'] 	 = 'Etiqueta de Palabras Claves';
$_['entry_meta_description'] = 'Etiqueta de Meta Descripción';
$_['entry_keyword']          = 'SEO Palabras Claves';
$_['entry_model']            = 'Modelo';
$_['entry_sku']              = 'SKU';
$_['entry_upc']              = 'UPC';
$_['entry_ean']              = 'EAN';
$_['entry_jan']              = 'JAN';
$_['entry_isbn']             = 'ISBN';
$_['entry_mpn']              = 'MPN';
$_['entry_location']         = 'Localización';
$_['entry_shipping']         = 'Requiere Envío';
$_['entry_manufacturer']     = 'Fabricante';
$_['entry_store']            = 'Tiendas';
$_['entry_date_available']   = 'Disponible desde fecha';
$_['entry_quantity']         = 'Cantidad';
$_['entry_minimum']          = 'Cantidad Mínima';
$_['entry_stock_status']     = 'Estatus Fuera de Stock';
$_['entry_price']            = 'Precio';
$_['entry_tax_class']        = 'Tipo de Impuesto';
$_['entry_points']           = 'Puntos';
$_['entry_option_points']    = 'Puntos';
$_['entry_subtract']         = 'Restar del Stock';
$_['entry_weight_class']     = 'Medida de Peso';
$_['entry_weight']           = 'Peso';
$_['entry_dimension']        = 'Dimensiones (L x A x A)';
$_['entry_length_class']     = 'Tipo de Longitud';
$_['entry_length']           = 'Longitud';
$_['entry_width']            = 'Ancho';
$_['entry_height']           = 'Alto';
$_['entry_image']            = 'Imagen';
$_['entry_customer_group']   = 'Grupo de Clientes';
$_['entry_date_start']       = 'Fecha de Inicio';
$_['entry_date_end']         = 'Fecha Final';
$_['entry_priority']         = 'Prioridad';
$_['entry_attribute']        = 'Atributo';
$_['entry_attribute_group']  = 'Grupo de atributos';
$_['entry_text']             = 'Texto';
$_['entry_option']           = 'Opción';
$_['entry_option_value']     = 'Valor de la Opción';
$_['entry_required']         = 'Requerido';
$_['entry_status']           = 'Estado';
$_['entry_sort_order']       = 'Ordenar por';
$_['entry_category']         = 'Categorías';
$_['entry_filter']           = 'Filtros';
$_['entry_download']         = 'Descargas';
$_['entry_related']          = 'Productos Relacionados';
$_['entry_tag']          	 = 'Etiquetas del Producto';
$_['entry_reward']           = 'Puntos de Recompensa';
$_['entry_layout']           = 'Anulación de la disposición';
$_['entry_recurring']        = 'Perfil Recurrente';

// Help
$_['help_keyword']           = 'No utilice espacios, en su lugar utilice el guión - y asegúrese que la palabra clave es única.';
$_['help_sku']               = 'Número de Referencia';
$_['help_upc']               = 'Código Universal del Producto';
$_['help_ean']               = 'Número Europeo del Artículo';
$_['help_jan']               = 'Número Japonés del Artículo';
$_['help_isbn']              = 'Número Internacional Estándar';
$_['help_mpn']               = 'Número de Parte del Fabricante';
$_['help_manufacturer']      = '(Autocompletar)';
$_['help_minimum']           = 'Forzar una cantidad mínima de pedido';
$_['help_stock_status']      = 'Estado que se muestra cuando un producto está fuera de stock';
$_['help_points']            = 'Número de puntos necesarios para comprar este artículo. Si no quieres que se adquiera con punto dejar en 0.';
$_['help_category']          = '(Autocompletar)';
$_['help_filter']            = '(Autocompletar)';
$_['help_download']          = '(Autocompletar)';
$_['help_related']           = '(Autocompletar)';
$_['help_tag']          	 = 'Coma Separado';

// Error
$_['error_warning']          = '¡Advertencia: Por favor revise cuidadosamente los errores en el formulario!';
$_['error_permission']       = '¡Advertencia: No tienes permiso para modificar productos!';
$_['error_name']             = '¡El Nombre del producto debe ser mayor que 3 y menor que 255 caracteres!';
$_['error_meta_title']       = '¡Meta Título debe ser mayor a 3 y menor a 255 caracteres!';
$_['error_model']            = '¡El Modelo del Producto debe ser mayor que 1 y menor que 64 caracteres!';
$_['error_keyword']          = '¡Palabra clave SEO ya en uso!';