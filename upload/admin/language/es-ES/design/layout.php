<?php
// Heading
$_['heading_title']       = 'Diseños';

// Text
$_['text_success']        = 'Ha modificado el diseño satisfactoriamente!';
$_['text_list']           = 'Lista de Diseños';
$_['text_add']            = 'Añade Diseño';
$_['text_edit']           = 'Edita Diseño';
$_['text_default']        = 'Predefinido';
$_['text_content_top']    = 'Contenido Superior';
$_['text_content_bottom'] = 'Contenido Inferior';
$_['text_column_left']    = 'Columna Izquierda';
$_['text_column_right']   = 'Columna Derecha';

// Column
$_['column_name']         = 'Nombre del Diseño';
$_['column_action']       = 'Acción';

// Entry
$_['entry_name']          = 'Nombre del Diseño';
$_['entry_store']         = 'Tienda';
$_['entry_route']         = 'Ruta';
$_['entry_module']        = 'Módulo';
$_['entry_position']      = 'Posición';
$_['entry_sort_order']    = 'Ordenar por';

// Error
$_['error_permission']    = '¡Advertencia: No tienes permiso para modificar diseños!';
$_['error_name']          = 'El nombre del diseño debe estar entre 3 y 64 caracteres!';
$_['error_default']       = '¡Advertencia: Esta disposición no se puede eliminar ya que actualmente está asignada como el diseño de la tienda por defecto!';
$_['error_store']         = '¡Advertencia: Esta disposición no se puede eliminar ya que actualmente está asignada a %s tiendas!';
$_['error_product']       = '¡Advertencia: Esta disposición no se puede eliminar ya que actualmente está asignada a los %s productos!';
$_['error_category']      = '¡Advertencia: Esta disposición no se puede eliminar ya que actualmente está asignada a %s categorías!';
$_['error_information']   = '¡Advertencia: Esta disposición no se puede eliminar ya que actualmente está asignada a %s páginas de información!';