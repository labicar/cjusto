<?php
// Heading
$_['heading_title']    = 'Puntos de Recompensa';

// Text
$_['text_total']       = 'Totales del Pedido';
$_['text_success']     = 'Éxito: Ha modificado el total para los puntos de recompensa!';
$_['text_edit']        = 'Editar puntos de recompensa Total';

// Entry
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Orden de aparición';

// Error
$_['error_permission'] = 'ADVERTENCIA: No tienes permiso para modificar el total de los puntos de recompensa!';