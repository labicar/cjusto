<?php
// Heading
$_['heading_title']    = 'Cargo Klarna (Aplica en Suecia)';

// Text
$_['text_total']       = 'Totales del Pedido';
$_['text_success']     = 'Éxito: Ha modificado el cargo total de Klarna!';
$_['text_edit']        = 'Editar el cargo total de Klarna';
$_['text_sweden']      = 'Suecia';
$_['text_norway']      = 'Noruega';
$_['text_finland']     = 'Finlandia';
$_['text_denmark']     = 'Dinamarca';
$_['text_germany']     = 'Alemania';
$_['text_netherlands'] = 'Países Bajos';

// Entry
$_['entry_total']      = 'Total del pedido';
$_['entry_fee']        = 'Cargo por factura';
$_['entry_tax_class']  = 'Tipo de Impuesto';
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Ordenar por';

// Error
$_['error_permission'] = 'ADVERTENCIA: No tienes permiso para modificar el cargo total de Klarna!';