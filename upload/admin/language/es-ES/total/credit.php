<?php
// Heading
$_['heading_title']    = 'Crédito en la tienda';

// Text
$_['text_total']       = 'Totales del Pedido';
$_['text_success']     = 'Éxito: Ha modificado el total de crédito de tienda!';
$_['text_edit']        = 'Editar total de crédito de tienda';

// Entry
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Ordenar por';

// Error
$_['error_permission'] = 'ADVERTENCIA: No tienes permiso para modificar el crédito total en la tienda!';