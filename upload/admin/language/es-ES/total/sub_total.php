<?php
// Heading
$_['heading_title']    = 'Subtotal';

// Text
$_['text_total']       = 'Totales del Pedido';
$_['text_success']     = 'Éxito: Ha modificado sub total total!';
$_['text_edit']        = 'Editar Total sub-total';

// Entry
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Ordenar por';

// Error
$_['error_permission'] = 'ADVERTENCIA: No tienes permiso para modificar sub total total!';