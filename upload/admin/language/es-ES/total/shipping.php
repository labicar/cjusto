<?php
// Heading
$_['heading_title']    = 'Gastos de Envío';

// Text
$_['text_total']       = 'Totales del Pedido';
$_['text_success']     = 'Éxito: Ha modificado envío total!';
$_['text_edit']        = 'Editar envío Total';

// Entry
$_['entry_estimator']  = 'Estimador de gastos de envío';
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Ordenar por';

// Error
$_['error_permission'] = 'ADVERTENCIA: No tienes permiso para modificar envío total!';