<?php
// Heading
$_['heading_title']     = 'Usuarios';

// Text
$_['text_success']      = 'Éxito: ¡Ha modificado los usuarios!';
$_['text_list']         = 'Lista de usuarios';
$_['text_add']          = 'Agregar Usuario';
$_['text_edit']         = 'Editar usuario';

// Column
$_['column_username']   = 'Usuario';
$_['column_status']     = 'Estado';
$_['column_date_added'] = 'Agregar Fecha';
$_['column_action']     = 'Acción';

// Entry
$_['entry_username']   	= 'Usuario';
$_['entry_user_group'] 	= 'Grupos de Usuarios';
$_['entry_password']   	= 'Contraseña';
$_['entry_confirm']    	= 'Confirmar';
$_['entry_firstname']  	= 'Nombre';
$_['entry_lastname']   	= 'Apellidos';
$_['entry_email']      	= 'E-Mail';
$_['entry_image']      	= 'Imagen';
$_['entry_status']     	= 'Estado';

// Error
$_['error_permission'] 	= 'ADVERTENCIA: No tienes permiso para modificar los usuarios!';
$_['error_account']    	= 'ADVERTENCIA: No puede eliminar su propia cuenta.';
$_['error_exists']     	= 'ADVERTENCIA: El usuario ya está en uso!';
$_['error_username']   	= '¡Nombre de usuario debe tener entre 3 y 20 caracteres!';
$_['error_password']   	= 'La Contraseña debe poseer entre 4 y 20 caracteres!';
$_['error_confirm']    	= 'La contraseña y su confirmación no coinciden!';
$_['error_firstname']  	= 'El nombre debe tener entre 1 y 21 caracteres!';
$_['error_lastname']   	= 'El apellido debe tener entre 1 y 21 caracteres!';