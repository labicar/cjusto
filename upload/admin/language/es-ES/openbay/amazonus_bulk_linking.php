<?php
// Heading
$_['heading_title'] 				= 'Enlace masivo';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Amazon USA';

// Button
$_['button_load'] 					= 'Cargar';
$_['button_link'] 					= 'Vínculo';

// Text
$_['text_local'] 					= 'Local';
$_['text_load_listings'] 			= "Cargar todos sus listados de Amazon puede tardar algún tiempo (hasta 2 horas en algunos casos). Si se enlaza con sus artículos, los stocks en Amazon se actualizarán con los stocks de su tienda.";
$_['text_report_requested'] 		= 'Se ha solicitado con éxito el Listado de Amazon';
$_['text_report_request_failed'] 	= 'No se ha podido solicitar listado';
$_['text_loading'] 					= 'Cargando artículos';

// Column
$_['column_asin'] 					= 'ASIN (Amazon Standard Identification Number)';
$_['column_price'] 					= 'Precio';
$_['column_name'] 					= 'Nombre';
$_['column_sku'] 					= 'SKU';
$_['column_quantity'] 				= 'Cantidad';
$_['column_combination'] 			= 'Cantidad';

// Error
$_['error_bulk_link_permission'] 	= 'Enlazan a granel no está disponible en tu plan, por favor actualizar para usar esta característica.';