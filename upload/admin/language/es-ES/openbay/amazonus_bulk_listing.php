<?php
// Heading
$_['heading_title'] 				= 'Listado masivo';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Amazon US';

// Text
$_['text_searching'] 				= 'Buscando';
$_['text_finished'] 				= 'Completado';
$_['text_dont_list'] 				= 'No listar';
$_['text_listing_values'] 			= 'Valores del listado';
$_['text_new'] 						= 'Nuevo';
$_['text_used_like_new'] 			= 'Usado - Como nuevo';
$_['text_used_very_good'] 			= 'Usado - Muy bueno';
$_['text_used_good'] 				= 'Usado - Bueno';
$_['text_used_acceptable'] 			= 'Usado - Aceptable';
$_['text_collectible_like_new'] 	= 'Coleccionables - Como nuevo';
$_['text_collectible_very_good'] 	= 'Coleccionables - Muy bueno';
$_['text_collectible_good'] 		= 'Coleccionables - Bueno';
$_['text_collectible_acceptable'] 	= 'Coleccionables - Aceptable';
$_['text_refurbished'] 				= 'Restaurado';

// Entry
$_['entry_condition'] 				= 'Estado';
$_['entry_condition_note'] 			= 'Nota de estado';
$_['entry_start_selling'] 			= 'Empezar a vender';

// Column
$_['column_name'] 					= 'Nombre';
$_['column_image'] 					= 'Imagen';
$_['column_model'] 					= 'Modelo';
$_['column_status'] 				= 'Estado';
$_['column_matches'] 				= 'Coincidencias';
$_['column_result'] 				= 'Resultado';

// Button
$_['button_list'] 					= 'Lista';

// Error
$_['error_product_sku'] 			= 'El producto debe tener un SKU';
$_['error_searchable_fields'] 		= 'EL producto debe tener campo ISBN, Código EAN, UPC o JAN relleno';
$_['error_bulk_listing_permission'] = 'Listado masivo no está permitido en su plan, por favor actualícelo';
$_['error_select_items'] 			= 'Debe seleccionar al menos 1 artículo para buscar';