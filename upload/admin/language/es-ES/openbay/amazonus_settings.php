<?php
// Heading
$_['heading_title']        				= 'Configuración del mercado';
$_['text_openbay']						= 'OpenBay Pro';
$_['text_amazon']						= 'Amazon US';

// Text
$_['text_api_status']               	= 'Estado de la conexión del API';
$_['text_api_ok']                   	= 'Conexión OK, Auth OK';
$_['text_api_auth_error']           	= 'Conexión OK, Auth falló';
$_['text_api_error']                	= 'Error en conexión';
$_['text_order_statuses']           	= 'Estado de pedidos';
$_['text_unshipped']                	= 'No enviado';
$_['text_partially_shipped']        	= 'Parcialmente enviado';
$_['text_shipped']                  	= 'Enviado';
$_['text_canceled']                 	= 'Cancelado';
$_['text_other']                    	= 'Otro';
$_['text_marketplaces']             	= 'Mercados';
$_['text_setttings_updated']        	= 'LA configuración se actualizó correctamente.';
$_['text_new'] 							= 'Nuevo';
$_['text_used_like_new'] 				= 'Usado - Como nuevo';
$_['text_used_very_good'] 				= 'Usado - Muy bueno';
$_['text_used_good'] 					= 'Usado - Bueno';
$_['text_used_acceptable'] 				= 'Usado - Aceptable';
$_['text_collectible_like_new'] 		= 'Coleccionables - Como nuevo';
$_['text_collectible_very_good'] 		= 'Coleccionables - Muy bueno';
$_['text_collectible_good'] 			= 'Coleccionables - Bueno';
$_['text_collectible_acceptable'] 		= 'Coleccionables - Aceptable';
$_['text_refurbished'] 					= 'Restaurado';

// Error
$_['error_permission']         			= 'No tiene acceso a este módulo';

// Entry
$_['entry_status']                 		= 'Estado';
$_['entry_token']						= 'Token';
$_['entry_string1']              		= 'Cifrado cadena 1';
$_['entry_string2']              		= 'Cifrado cadena 2';
$_['entry_import_tax']              	= 'Impuesto de los artículos importados';
$_['entry_customer_group']          	= 'Grupo de Clientes';
$_['entry_tax_percentage']           	= 'Porcentaje añadido al precio del producto por defecto';
$_['entry_default_condition']        	= 'Estado del producto predeterminado';
$_['entry_notify_admin']             	= 'Notificar al admin por pedido nuevo';
$_['entry_default_shipping']         	= 'Envío predetermindado';

// Tabs
$_['tab_settings']            			= 'Detalles de la API';
$_['tab_listing']                  		= 'Listados';
$_['tab_orders']                   		= 'Pedidos';

// Help
$_['help_import_tax']          			= 'Usado si Amazon no devuelve información tributaria';
$_['help_customer_group']      			= 'Seleccione un grupo de clientes para asignar pedidos importados';
$_['help_default_shipping']    			= 'Utiliza la opción preseleccionada en la actualización de los pedidos';
$_['help_tax_percentage']           	= 'Porcentaje añadido al precio del producto por defecto';