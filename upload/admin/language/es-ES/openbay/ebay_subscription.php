<?php
// Heading
$_['heading_title']             = 'Suscripción';
$_['text_openbay']              = 'OpenBay Pro';
$_['text_ebay']                 = 'eBay';

// Buttons
$_['button_plan_change']  		= 'Cambiar de plan';

// Columns
$_['column_plan']  				= 'Nombre del plan';
$_['column_call_limit']  		= 'Límite de llamada';
$_['column_price']  			= 'Precio (por mes)';
$_['column_description']  		= 'Descripción';
$_['column_current']  			= 'Plan actual';

// Text
$_['text_subscription_current'] = 'Plan actual';
$_['text_subscription_avail']   = 'Planes disponibles';
$_['text_subscription_avail1']  = 'Cambio de planes sera inmediato y las llamadas no utilizadas no serán acreditadas.';
$_['text_subscription_avail2']  = 'Para downgrade al plan básico por favor cancele su suscripción activa en PayPal.';
$_['text_ajax_acc_load_plan']   = 'ID de suscripción de PayPal: ';
$_['text_ajax_acc_load_plan2']  = ', usted debería cancelar TODAS las otras suscripciones de nosotros';
$_['text_load_my_plan']         = 'Cargando su plan';
$_['text_load_plans']           = 'Cargando planes disponibles';

// Errors
$_['error_ajax_load']      		= 'Lo sentimos, no se pudo obtener una respuesta. Inténtalo más tarde.';