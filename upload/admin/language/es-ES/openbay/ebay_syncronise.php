<?php
// Heading
$_['heading_title']           	= 'Sincronizar';
$_['text_openbay']              = 'OpenBay Pro';
$_['text_ebay']                 = 'eBay';

// Buttons
$_['button_update']				= 'Actualizar';

// Entry
$_['entry_sync_categories']     = 'Obtener categorías principales de eBay';
$_['entry_sync_shop']        	= 'Consigue tu categoría de compras';
$_['entry_sync_setting']     	= 'Obtener la configuración';

// Text
$_['text_complete']             = 'Completo';
$_['text_sync_desc']            = 'Sincronice su tienda con las ultimas opciones disponibles de envío y categoría de eBay, estos datos son solo para las opciones cuando se incluye un articulo en eBay: no importará categorías a su tienda, etc.';
$_['text_ebay_categories'] 		= 'Esto puede tomar un tiempo, espere 5 minutos antes de hacer cualquier otra cosa.';
$_['text_category_import']      = 'Las categorías de la tienda de eBay se han importado.';
$_['text_setting_import']  		= 'Los ajustes se han importado.';

// Help
$_['help_sync_categories']   	= '¡Esto no importa cualquier categoría a su tienda!';
$_['help_sync_shop']   			= '¡Esto no importa cualquier categoría a su tienda!';
$_['help_sync_setting']			= 'Esto importa tipos de pago, envío, ubicación y más.';

// Errors
$_['error_settings']			= 'Ocurrió un error al cargar la configuración.';
$_['error_failed']              = 'Error al cargar';