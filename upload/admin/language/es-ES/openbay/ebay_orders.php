<?php
// Heading
$_['heading_title']					= 'Revisar anuncio de eBay';
$_['text_openbay']					= 'OpenBay Pro';
$_['text_ebay']						= 'eBay';

// Buttons
$_['button_pull_orders']        	= 'Comienzar';

// Entry
$_['entry_pull_orders']             = 'Sacar nuevos pedidos';

// Text
$_['text_sync_pull_notice']         = 'Esto generará nuevos pedidos desde el último control automatizado.. Si lo acaba de instalar, se utilizara de manera predeterminada en las últimas 24 horas.';
$_['text_ajax_orders_import']       = 'Cualquier orden nueva debe aparecer en unos minutos';
$_['text_complete']             	= 'Importe solicitado';
$_['text_failed']               	= 'Error al cargar';

// Errors
$_['error_validation']             = 'Necesita registrarse para su token API y activar el módulo.';