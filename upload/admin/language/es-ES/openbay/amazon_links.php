<?php
// Heading
$_['heading_title']					= 'Enlaces de producto';
$_['text_openbay']					= 'OpenBay Pro';
$_['text_amazon']					= 'Amazon EU';

// Text
$_['text_desc1']                    = 'Enlazar sus productos permitirá control de stock en sus listados de Amazon.';
$_['text_desc2'] 					= 'El stock de cada artículo que sea actualizado localmente (en su tienda OpenCart) actualizará su listado en Amazon';
$_['text_desc3']                    = 'Puede enlazar artículos manualmente escribiendo el SKU de Amazon y el nombre del producto o cargar todos los productos no enlazados y luego escribir el SKU de amazon. (Al cargar productos de OpenCart a Amazon, se agregarán automáticamente los enlaces)';
$_['text_new_link']                 = 'Nuevo enlace';
$_['text_autocomplete_product']     = 'Producto (Auto completa del nombre)';
$_['text_amazon_sku']               = 'SKU de Amazon';
$_['text_action']                   = 'Acción';
$_['text_linked_items']             = 'Artículos enlazados';
$_['text_unlinked_items']           = 'Artículos no enlazados';
$_['text_name']                     = 'Nombre';
$_['text_model']                    = 'Modelo';
$_['text_combination']              = 'Cantidad';
$_['text_sku']                      = 'SKU';
$_['text_amazon_sku']               = 'SKU de Amazon';

// Button
$_['button_load']                 	= 'Cargar';

// Error
$_['error_empty_sku']        		= '¡SKU de Amazon no puede estar vacío!';
$_['error_empty_name']       		= '¡Nombre del producto no puede estar vacío!';
$_['error_no_product_exists']       = 'El producto no existe. Por favor, use los valores de auto completado.';