<?php
// Heading
$_['heading_title'] 				= 'Listados guardados';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Amazon US';

// Text
$_['text_description']              = 'Esta es la lista de  productos que están guardados y listos para ser subidos al Amazon.';
$_['text_uploaded_alert']           = '¡Listado(s) guardados han sido subidos!';
$_['text_delete_confirm']           = '¿Esta seguro?';
$_['text_complete']           		= 'Listas subidas';

// Column
$_['column_name']              		= 'Nombre';
$_['column_model']             		= 'Modelo';
$_['column_sku']               		= 'SKU';
$_['column_amazon_sku']        		= 'SKU de Amazon';
$_['column_action']           		= 'Acción';