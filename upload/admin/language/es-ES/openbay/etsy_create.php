<?php
// Headings
$_['heading_title']      	= 'Crear nuevo listado Etsy';
$_['text_title']      		= 'Etsy';
$_['text_openbay']      	= 'OpenBay Pro';

// Tabs
$_['tab_additional']      	= 'Información adicional 	';

// Text
$_['text_option']      		= 'Seleccionar opción';
$_['text_category_selected']= 'Categoría seleccionada';
$_['text_material_add']  	= 'Añadir material';
$_['text_material_remove']  = 'Quitar Material';
$_['text_tag_add']  		= 'Añadir etiqueta';
$_['text_tag_remove']  		= 'Eliminar etiqueta';
$_['text_created']  		= 'Tu anuncio ha sido creado';
$_['text_listing_id']  		= 'Listados de ID';
$_['text_img_upload']  		= 'Subiendo imagen';
$_['text_img_upload_done']  = 'Imagen cargada';

// Entry
$_['entry_title']      		= 'Título del producto';
$_['entry_description']     = 'Descripción';
$_['entry_price']      		= 'Precio';
$_['entry_non_taxable']     = 'No imponible';
$_['entry_category']     	= 'Categoría superior';
$_['entry_sub_category']    = 'Subcategoría';
$_['entry_sub_sub_category']= 'Sub subcategoría';
$_['entry_who_made']		= '¿Quién lo hizo?';
$_['entry_when_made']		= '¿Cuándo se hizo?';
$_['entry_recipient']		= '¿Para quién es?';
$_['entry_occasion']		= '¿Para que ocasión es?';
$_['entry_is_supply']		= '¿Es esto una fuente?';
$_['entry_state']      		= 'Estado de listado';
$_['entry_style']      		= 'Etiqueta de estilo 1';
$_['entry_style_2']      	= 'Etiqueta de estilo 2';
$_['entry_processing_min']  = 'Min de tiempo de procesamiento';
$_['entry_processing_max']  = 'Tiempo de procesamiento maximo';
$_['entry_materials']  		= 'Materiales';
$_['entry_tags']  			= 'Etiquetas';
$_['entry_shipping']  		= 'Perfil de envío';
$_['entry_shop']  			= 'Sección de la tienda';
$_['entry_is_custom']  		= '¿Puede ser personalizado?';
$_['entry_image']  			= 'Imagen principal';
$_['entry_image_other']		= 'Otras imágenes';

// Errors
$_['error_no_shipping']  	= '¡No tienes configurado ningún perfil de envío!';
$_['error_no_shop_secton']  = '¡No tienes configurado ninguna sección de compra!';
$_['error_no_img_url']  	= 'Ninguna imagen seleccionada para descargar';
$_['error_no_listing_id']  	= 'No hay suministro de ID';
$_['error_title_length']  	= 'Título es demasiado largo';
$_['error_title_missing']  	= 'No se encuentra el titulo';
$_['error_desc_missing']  	= 'La descripción está perdida o vacia';
$_['error_price_missing']  	= 'El precio está perdido o vacío';
$_['error_category']  		= 'No ha seleccionado categoría';
$_['error_style_1_tag']  	= 'No es valida el estilo de etiqueta 1';
$_['error_style_2_tag']  	= 'No es valida el estilo de etiqueta 2';
$_['error_materials']  		= 'Sólo puede agregar 13 materiales';
$_['error_tags']  			= 'Sólo puede agregar 13 etiquetas';