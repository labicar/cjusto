<?php
// Heading
$_['heading_title']        				= 'Actualización de Stocks';
$_['text_openbay']						= 'OpenBay Pro';
$_['text_amazon']						= 'Amazon EU';

// Text
$_['text_empty']                    	= '¡No hay resultados!';

// Entry
$_['entry_date_start']               	= 'Fecha de Inicio';
$_['entry_date_end']                 	= 'Fecha Final';

// Column
$_['column_ref']                      	= 'Referencia';
$_['column_date_requested']           	= 'Fecha de solicitud';
$_['column_date_updated']             	= 'Fecha de actualización';
$_['column_status']                   	= 'Estado';
$_['column_sku']                      	= 'Amazon SKU';
$_['column_stock']                    	= 'Stock';