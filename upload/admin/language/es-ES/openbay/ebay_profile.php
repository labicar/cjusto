<?php
// Heading
$_['heading_title']                     = 'Perfiles';
$_['text_openbay']                      = 'OpenBay Pro';
$_['text_ebay']                         = 'eBay';

//Tabs
$_['tab_returns']          				= 'Devoluciones';
$_['tab_template']         				= 'Plantilla';
$_['tab_gallery']          				= 'Galería';
$_['tab_settings']         				= 'Configuración';

//Shipping Profile
$_['text_shipping_dispatch_country']    = 'Envío de país';
$_['text_shipping_postcode']            = 'Código postal de la ubicación';
$_['text_shipping_location']            = 'Ciudad / Estado';
$_['text_shipping_despatch']            = 'Tiempo de envío';
$_['text_shipping_despatch_help']       = 'Este es el número máximo de días que llevará a enviar el artículo';
$_['text_shipping_nat']                 = 'Servicios de transporte nacional';
$_['text_shipping_intnat']              = 'Servicios de transporte internacional';
$_['text_shipping_first']               = 'Primer elemento';
$_['text_shipping_add']                 = 'Elementos adicionales';
$_['text_shipping_service']             = 'Servicio';
$_['text_shipping_in_desc']             = 'Información de carga en la descripción';
$_['text_shipping_getitfast']           = 'Consíguelo rápido!';
$_['text_shipping_zones']               = 'Enviar a las zonas';
$_['text_shipping_worldwide']           = 'Todo el mundo';
$_['text_shipping_type_nat']           	= 'Envío nacional';
$_['text_shipping_type_int']           	= 'Envío internacional';
$_['text_shipping_flat']           		= 'Tarifa plana';
$_['text_shipping_calculated']          = 'Calculado';
$_['text_shipping_freight']          	= 'Transporte';
$_['text_shipping_handling']          	= 'Gastos de tramitación';
$_['text_shipping_cod']           		= 'Honorarios por efectivo contra entrega';
$_['text_shipping_handling_nat']    	= 'Tarifa de manipulación (nacional)';
$_['entry_shipping_handling_int']    	= 'Cuota de manejo (internacional)';

//Returns profile
$_['text_returns_accept']       		= 'Devoluciones aceptadas';
$_['text_returns_inst']         		= 'Política de devoluciones';
$_['text_returns_days']         		= 'Dias de devolución';
$_['text_returns_days10']       		= '10 días';
$_['text_returns_days14']       		= '14 días';
$_['text_returns_days30']       		= '30 días';
$_['text_returns_days60']       		= '60 días';
$_['text_returns_type']         		= 'Tipo de devolución';
$_['text_returns_type_money']   		= 'Devoluciones';
$_['text_returns_type_exch']    		= 'Devolución o intercambio';
$_['text_returns_costs']        		= 'Devolver los gastos de envío';
$_['text_returns_costs_b']      		= 'El comprador paga';
$_['text_returns_costs_s']      		= 'El vendedor paga';
$_['text_returns_restock']      		= 'Tasa de reposición';

//Template profile
$_['text_template_choose']      		= 'Plantilla predeterminada';
$_['text_template_choose_help'] 		= 'Una plantilla predeterminada se cargará automáticamente en el listado para ahorrar tiempo';
$_['text_image_gallery']        		= 'Tamaño de imagen de la galería';
$_['text_image_gallery_help']   		= 'Tamaño del pixel de la galería de imágenes que se añaden a la plantilla.';
$_['text_image_thumb']          		= 'Tamaño de miniatura';
$_['text_image_thumb_help']     		= 'Tamaño de pixel de las imágenes de la miniatura que se añaden a su plantilla.';
$_['text_image_super']          		= 'Imágenes de tamaño grande';
$_['text_image_gallery_plus']   		= 'Galería extra';
$_['text_image_all_ebay']       		= 'Agregar todas las imágenes a eBay';
$_['text_image_all_template']   		= 'Agregar todas las imágenes a la plantilla';
$_['text_image_exclude_default']		= 'Excluir imagen predeterminada';
$_['text_image_exclude_default_help']	= '¡Solo para la función de listado masivo! No incluirá la imagen por defecto del producto en la lista de imágenes del tema';
$_['text_confirm_delete']       		= '¿Estás seguro de querer eliminar este perfil?';
$_['text_width']      					= 'Ancho';
$_['text_height']      					= 'Alto';
$_['text_px']      						= 'px';

//General profile
$_['text_general_private']      		= 'Elementos de lista como subasta privada';
$_['text_general_price']        		= '% Modificación de precio';
$_['text_general_price_help']   		= '0 es por defecto, -10 lo reducirá el 10%, 10 lo incrementara en un 10% (sólo para listado masivo)';

//General profile options
$_['text_profile_name']         		= 'Nombre';
$_['text_profile_default']      		= 'Predefinido';
$_['text_profile_type']         		= 'Tipo';
$_['text_profile_desc']         		= 'Descripción';
$_['text_profile_action']       		= 'Acción';

// Profile types
$_['text_type_shipping']       			= 'Envío';
$_['text_type_returns']       			= 'Devoluciones';
$_['text_type_template']       			= 'Plantilla &amp; galería';
$_['text_type_general']       			= 'Configuración general';

//Success messages
$_['text_added']                		= 'Un nuevo perfil ha sido añadido';
$_['text_updated']              		= 'El perfil ha sido actualizado';

//Errors
$_['error_permission']        			= 'No tienes permiso para editar perfiles';
$_['error_name']           				= 'Debe introducir un nombre de perfil';
$_['error_no_template']          		= 'ID de plantilla no existe';
$_['error_missing_settings'] 			= 'No puede agregar, editar o eliminar perfiles hasta que sincronice sus configuraciones de eBay';