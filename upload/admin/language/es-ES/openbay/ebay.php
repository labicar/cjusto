<?php
// Heading
$_['heading_title']         		= 'eBay';
$_['text_openbay']					= 'OpenBay Pro';
$_['text_dashboard']				= 'Panel de control de eBay';

// Text
$_['text_success']         			= 'Ha guardado los cambios a la extensión de eBay';
$_['text_heading_settings']         = 'Configuración';
$_['text_heading_sync']             = 'Sincronizar';
$_['text_heading_subscription']     = 'Cambiar Plan';
$_['text_heading_usage']          	= 'Uso';
$_['text_heading_links']            = 'Enlaces del Elemento';
$_['text_heading_item_import']      = 'Importar articulos';
$_['text_heading_order_import']     = 'Importar pedidos';
$_['text_heading_adds']             = 'Complementos instalados';
$_['text_heading_summary']          = 'Resumen de eBay';
$_['text_heading_profile']          = 'Perfiles';
$_['text_heading_template']         = 'Plantillas';
$_['text_heading_ebayacc']          = 'Cuenta de eBay';
$_['text_heading_register']         = 'Regístrese aquí';

// Error
$_['error_category_nosuggestions']  = 'No se pudo cargar las categorías sugeridas';
$_['error_loading_catalog']         = 'Ha fallado la búsqueda en el catalogo de eBay';
$_['error_generic_fail']         	= 'Se ha producido un Error desconocido!';