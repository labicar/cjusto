<?php
// Headings
$_['heading_title']        		= 'Configuración de Etsy';
$_['text_openbay']              = 'OpenBay Pro';
$_['text_etsy']                 = 'Etsy';

// Text
$_['text_success']     			= 'Su configuración ha sido guardada';
$_['text_status']         		= 'Estado';
$_['text_account_ok']  			= 'Conexión a Etsy OK';
$_['text_api_other']            = 'Enlaces';
$_['text_token_register']       = 'Registro';
$_['text_api_ok']       		= 'Conexión API OK';
$_['text_pull_orders']    		= 'Ordenes de extracción';
$_['text_complete']    			= 'Completo';
$_['text_failed']    			= 'Fallado';
$_['text_orders_imported']    	= 'La orden de extracción ha sido solicitada';
$_['text_api_status']           = 'Conexión API';

// Entry
$_['entry_import_def_id']       = '$_[\'entry_import_def_id\']
Entry';
$_['entry_import_paid_id']      = 'Estado de pago:';
$_['entry_import_shipped_id']   = 'Estado de envio:';
$_['entry_enc1']            	= '$_[\'entry_enc1\']';
$_['entry_enc2']            	= 'Cifrado de API 2';
$_['entry_token']            	= 'Token API';
$_['entry_address_format']      = 'Formato de dirección por defecto';

// Buttons
$_['button_pull']    			= 'hale el boton


';

// Error
$_['error_api_connect']         = 'Error al conectar a la API';
$_['error_account_info']    	= '$_ [\'error_account_info\'] ';

// Tabs
$_['tab_api_info']            	= 'Detalles de la API';

// Help
$_['help_address_format']  		= 'Solo se usa si el país ya no tiene una configuración de formato de dirección.';