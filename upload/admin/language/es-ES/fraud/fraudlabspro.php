<?php
// Heading
$_['heading_title']                           = 'FraudLabs Pro ID';

// Text
$_['text_fraud']                              = 'Anti Fraude';
$_['text_success']                            = 'Listo: Ha modificado las ordenes!';
$_['text_edit']                               = 'Configuración';
$_['text_signup']                             = 'FraudLabsPro es un servicio de detección de fraude. Si no tienes una clave de API puedes <a href="http://www.fraudlabspro.com/plan?ref=1730" target="_blank"> <u>registrarte aquí</u></a>.';
$_['text_id']                                 = 'FraudLabs Pro ID';
$_['text_ip_address']                         = 'Dirección IP';
$_['text_ip_net_speed']                       = 'Velocidad de Red IP';
$_['text_ip_isp_name']                        = 'Nombre de Proveedor ISP IP';
$_['text_ip_usage_type']                      = 'Tipo de Uso de IP';
$_['text_ip_domain']                          = 'Dominio de IP';
$_['text_ip_time_zone']                       = 'Zona Horaria de IP';
$_['text_ip_location']                        = 'Localización de IP';
$_['text_ip_distance']                        = 'Distancia de IP';
$_['text_ip_latitude']                        = 'Latitud IP';
$_['text_ip_longitude']                       = 'Longitud IP';
$_['text_risk_country']                       = 'País de alto riesgo';
$_['text_free_email']                         = 'Correo Gratuito';
$_['text_ship_forward']                       = 'Reenvío';
$_['text_using_proxy']                        = 'Usando Proxy';
$_['text_bin_found']                          = 'BIN encontrado';
$_['text_email_blacklist']                    = 'Lista Negra de Email';
$_['text_credit_card_blacklist']              = 'Lista Negra de Tarjetas de Crédito';
$_['text_score']                              = 'Puntuación FraudLabsPro';
$_['text_status']                             = 'Estado FraudLabs Pro';
$_['text_message']                            = 'Mensaje';
$_['text_transaction_id']                     = 'ID de la Transacción';
$_['text_credits']                     		  = 'Saldo';
$_['text_error']                              = 'Error:';
$_['text_flp_upgrade']                        = '<a href="http://www.fraudlabspro.com/plan" target="_blank">[Actualizar]</a>';
$_['text_flp_merchant_area']                  = 'Por favor ingresa a <a href="http://www.fraudlabspro.com/login" target="_blank"> FraudLabs Pro comerciante zona</a> para obtener más información acerca de esta orden.';


// Entry
$_['entry_status']                            = 'Estado';
$_['entry_key']                               = 'API Key';
$_['entry_score']                             = 'Puntuación de riesgo';
$_['entry_order_status']                      = 'Estado del pedido';
$_['entry_review_status']                     = 'Estado de Calificación';
$_['entry_approve_status']                    = 'Aprobar el estado de';
$_['entry_reject_status']                     = 'Rechazan estado';
$_['entry_simulate_ip']                       = 'Simular la IP';

// Help
$_['help_order_status']                       = 'Órdenes por encima del score establecido le será asignado el mismo status de ésta orden y no será permitido alcanzar el status de completado automáticamente.';
$_['help_review_status']                      = 'Órdenes que marcó como por FraudLabs Pro serán asignadas este estado de la orden.';
$_['help_approve_status']                     = 'Órdenes que marcó como por FraudLabs Pro serán asignadas este estado de la orden.';
$_['help_reject_status']                      = 'Órdenes que marcó como por FraudLabs Pro serán asignadas este estado de la orden.';
$_['help_simulate_ip']                        = 'Simular la dirección IP del visitante para la prueba. Deje en blanco para el ciclo de producción.';
$_['help_fraudlabspro_id']                    = 'Identificador único para identificar una transacción mediante el sistema FraudLabs Pro.';
$_['help_ip_address']                         = 'Dirección IP.';
$_['help_ip_net_speed']                       = 'Velocidad de conexión';
$_['help_ip_isp_name']                        = 'Ciudad Estiama de la dirección IP.';
$_['help_ip_usage_type']                      = 'Calcula el tipo de uso de la dirección IP. Por ejemplo, ISP, comercial, residencial.';
$_['help_ip_domain']                          = 'Dominio estimado de la dirección IP.';
$_['help_ip_time_zone']                       = 'Zona horaria estimada de la dirección IP.';
$_['help_ip_location']                        = 'Dominio estimado de la dirección IP.';
$_['help_ip_distance']                        = 'Distancia desde la dirección IP en lugar de facturación.';
$_['help_ip_latitude']                        = 'Latitud estimada de la dirección IP.';
$_['help_ip_longitude']                       = 'Longitud estimada de la dirección IP.';
$_['help_risk_country']                       = 'Si la dirección IP o el país de la dirección de facturación sea en Ghana, Nigeria o Vietnam.';
$_['help_free_email']                         = 'Si es de proveedor de correo electrónico gratuito.';
$_['help_ship_forward']                       = 'Si la dirección de envío esta en la base de datos de mails a omitir.';
$_['help_using_proxy']                        = 'Si la dirección IP es del servidor de Proxy anónimo.';
$_['help_bin_found']                          = 'Si la información de BIN coincide con la lista BIN.';
$_['help_email_blacklist']                    = 'Si la dirección de correo electrónico está en nuestra base de datos de lista negra.';
$_['help_credit_card_blacklist']              = 'Si la dirección de correo electrónico está en nuestra base de datos de lista negra.';
$_['help_score']                              = 'Puntuación de riesgo, 0 (bajo riesgo) - 100 (alto riesgo).';
$_['help_status']                             = 'Estado FraudLabs Pro.';
$_['help_message']                            = 'Descripción de mensaje de error de FraudLabs Pro.';
$_['help_transaction_id']                     = 'Haga clic en el enlace para ver el análisis del fraude de detalles.';
$_['help_credits']                            = 'Balance de las consultas en su cuenta después de esta transacción.';

// Error
$_['error_permission']                        = 'Advertencia: ¡No tienes permiso para modificar las opciones!';
$_['error_key']		                          = '¡Clave secreta necesaria!';