<?php
// Text
$_['text_approve_subject']      = '%s - su cuenta ha sido activada!';
$_['text_approve_welcome']      = '¡ Bienvenido y gracias por registrarse en %s!';
$_['text_approve_login']        = 'Se ha creado su cuenta y puede iniciar sesión utilizando su dirección de correo electrónico y contraseña visitando nuestro sitio web o en la siguiente dirección:';
$_['text_approve_services']     = 'Al iniciar sesión, usted podrá acceder a otros servicios, incluyendo revisar pedidos anteriores, imprimir facturas y editar la información de su cuenta.';
$_['text_approve_thanks']       = 'Gracias,';
$_['text_transaction_subject']  = '%s - crédito';
$_['text_transaction_received'] = 'Ha recibido crédito de %s!';
$_['text_transaction_total']    = 'Your total amount of credit is now %s.' . "\n\n" . 'Your account credit will be automatically deducted from your next purchase.';
$_['text_reward_subject']       = '%s - puntos de recompensa';
$_['text_reward_received']      = 'Ha recibido %s puntos de recompensa!';
$_['text_reward_total']         = 'Dispone de un total de %s puntos de recompensa.';