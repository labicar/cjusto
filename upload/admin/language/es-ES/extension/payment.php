<?php
// Heading
$_['heading_title']     = 'Pagos';

// Text
$_['text_success']      = '¡Ha modificado exitosamente los pagos!';
$_['text_list']         = 'Lista de Pagos';

// Column
$_['column_name']       = 'Modalidad de Pago';
$_['column_status']     = 'Estado';
$_['column_sort_order'] = 'Ordenar por';
$_['column_action']     = 'Acción';

// Error
$_['error_permission']  = '¡Advertencia: No tienes permiso para modificar los pagos!';