<?php
// Heading
$_['heading_title']        				= 'OpenBay Pro';

// Buttons
$_['button_retry']						= 'Reintentar';
$_['button_update']						= 'Actualizar';
$_['button_patch']						= 'Parche';
$_['button_ftp_test']					= 'Probar conexión';
$_['button_faq']						= 'Ver Preguntas Frecuentes';

// Tab
$_['tab_setting']						= 'Configuración';
$_['tab_update']						= 'Actualizaciones de software';
$_['tab_update_v1']						= 'Actualizador fácil';
$_['tab_update_v2']						= 'Actualizador compatible';
$_['tab_patch']							= 'Parche';
$_['tab_developer']						= 'Desarrollador';

// Text
$_['text_dashboard']         			= 'Panel de Control';
$_['text_success']         				= 'Los Ajustes han sido salvados correctamente';
$_['text_products']          			= 'Items';
$_['text_orders']          				= 'Pedidos';
$_['text_manage']          				= 'Administrar';
$_['text_help']                     	= 'Ayuda';
$_['text_tutorials']                    = 'Tutoriales';
$_['text_suggestions']                  = 'Ideas';
$_['text_version_latest']               = 'Está ejecutando la versión más reciente';
$_['text_version_check']     			= 'Comprobando versión de software';
$_['text_version_installed']    		= 'Versión instalada de OpenBay Pro: v';
$_['text_version_current']        		= 'Su versión es';
$_['text_version_available']        	= 'el último es';
$_['text_language']             		= 'Idioma respuesta API';
$_['text_getting_messages']     		= 'Recibiendo  mensajes de OpenBay Pro';
$_['text_complete']     				= 'Completo';
$_['text_test_connection']              = 'Prueba de conexión FTP';
$_['text_run_update']           		= 'Ejecute actualización';
$_['text_patch_complete']           	= 'Se ha aplicado el parche';
$_['text_connection_ok']				= 'Conectado al servidor OK. OpenCart carpetas encontradas';
$_['text_updated']						= 'Módulo ha sido actualizado (v.%s)';
$_['text_update_description']			= 'El actualizador hará cambios a su tienda. Asegúrese de tener una copia de seguridad completa y de la base de datos antes de actualizar.';
$_['text_patch_description']			= 'Si ha subido los archivos de actualización manualmente, debe ejecutar el parche para completar la actualización';
$_['text_clear_faq']                    = 'Ocultar popups de Preguntas Frecuentes';
$_['text_clear_faq_complete']           = 'Notificaciones se mostrará otra vez';
$_['text_install_success']              = 'Mercado ha sido instalado';
$_['text_uninstall_success']            = 'Mercado ha sido eliminado';
$_['text_title_messages']               = 'Mensajes &amp; notificaciones';
$_['text_marketplace_shipped']			= 'Se actualizará el estado del pedido enviado al mercado';
$_['text_action_warning']				= 'Esta acción es peligrosa así que está protegida con contraseña.';
$_['text_check_new']					= 'Comprobando si hay una versión más reciente';
$_['text_downloading']					= 'Descargando archivos de actualización';
$_['text_extracting']					= 'Extrayendo archivos';
$_['text_running_patch']				= 'Ejecutando parche';
$_['text_fail_patch']					= 'No se pueden extraer los archivos de actualización';
$_['text_updated_ok']					= 'Actualización completada, la versión instalada es ahora ';
$_['text_check_server']					= 'Comprobando requisitos del servidor';
$_['text_version_ok']					= 'El programa está actualizado, la versión instalada es ';
$_['text_remove_files']					= 'Eliminando archivos innecesarios';
$_['text_confirm_backup']				= 'Asegúrese de que tiene una copia de seguridad completa antes de continuar';

// Column
$_['column_name']          				= 'Nombre del Plugin';
$_['column_status']        				= 'Estado';
$_['column_action']        				= 'Acción';

// Entry
$_['entry_patch']            			= 'Parche de actualización manual';
$_['entry_ftp_username']				= 'Nombre de usuario FTP';
$_['entry_ftp_password']				= 'Contraseña FTP';
$_['entry_ftp_server']					= 'Dirección del servidor FTP';
$_['entry_ftp_root']					= 'Ruta a servidor FTP';
$_['entry_ftp_admin']            		= 'Directorio Admin';
$_['entry_ftp_pasv']                    = 'Modo PASV';
$_['entry_ftp_beta']             		= 'Utilizar la versión beta';
$_['entry_courier']						= 'Paquetería';
$_['entry_courier_other']           	= 'Otra Paquetería';
$_['entry_tracking']                	= 'Tracking #';
$_['entry_empty_data']					= '¿Borrar datos de la tienda?';
$_['entry_password_prompt']				= 'Por favor introduzca la contraseña de borrado de datos';
$_['entry_update']						= 'Actualización en 1 click';

// Error
$_['error_username']             		= 'Nombre de usuario FTP requerido';
$_['error_password']             		= 'Requerida contraseña FTP';
$_['error_server']               		= 'Servidor FTP requerido';
$_['error_admin']             			= 'Directorio admin esperado';
$_['error_no_admin']					= 'Conexión OK pero su directorio admin OpenCart no se encontró';
$_['error_no_files']					= '¡Conexión OK pero no se encontraron las carpetas de Opencart! ¿La ruta de acceso raíz es correcta?';
$_['error_ftp_login']					= 'No puede iniciar sesión con ese usuario';
$_['error_ftp_connect']					= 'No se pudo conectar al servidor';
$_['error_failed']						= 'Error al cargar, ¿reintentarlo?';
$_['error_tracking_id_format']			= 'Su tracking ID no puede contener los caracteres > o <';
$_['error_tracking_courier']			= 'Debe seleccionar a una paqueteria si desea agregar un tracking ID';
$_['error_tracking_custom']				= 'Por favor deje el campo vacío de paquetería si desea utilizar una personalizada';
$_['error_permission']					= 'No tienes permiso para modificar la extensión OpenBay Pro';
$_['error_mkdir']						= 'La función PHP mkdir está deshabilitada, póngase en contacto con su hosting';
$_['error_file_delete']					= 'No han podido ser eliminados los archivos, debe hacerlo manualmente';
$_['error_mcrypt']            			= 'Función PHP "mcrypt_encrypt" no está activada. Contacta a tu proveedor de hosting.';
$_['error_mbstring']               		= 'Librería de PHP "mb strings" no está activada. Contacta a tu proveedor de hosting.';
$_['error_ftpconnect']             		= 'Funciones FTP en PHP no están activadas. Contacta a tu proveedor de hosting.';
$_['error_oc_version']             		= 'Tu versión de OpenCart no ha sido verificada para su funcionamiento con éste módulo. Puede que experimentes problemas.';
$_['error_fopen']             			= 'Función PHP "fopen" esta desactivada por tu proveedor de host - no será posible importar imágenes cuando estés importando productos';

// Help
$_['help_ftp_username']           		= 'El nombre de usuario FTP de tu host';
$_['help_ftp_password']           		= 'Utilizar la contraseña FTP de tu host';
$_['help_ftp_server']      				= 'Dirección IP o nombre de dominio para tu servidor FTP';
$_['help_ftp_root']           			= '(Sin barra e.g. httpdocs/www)';
$_['help_ftp_admin']               		= 'Si has cambiado tu directorio admin actualizalo a la nueva ubicación';
$_['help_ftp_pasv']                    	= 'Cambiar tu conexión FTP en modo pasivo';
$_['help_ftp_beta']             		= '¡Precaución! La versión beta puede que no funcione correctamente';
$_['help_clear_faq']					= 'Mostrar todas las notificaciones de ayuda otra vez';
$_['help_empty_data']					= 'Esto puede causar graves daños, ¡no lo use si no sabe lo que hace!';
$_['help_easy_update']					= 'Haga clic en actualizar para instalar la última versión de OpenBay Pro automáticamente';
$_['help_patch']						= 'Haga clic para ejecutar el patch';