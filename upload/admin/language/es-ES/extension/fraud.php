<?php
// Heading
$_['heading_title']    = 'Anti-fraude';

// Text
$_['text_success']     = 'Listo: has modificado el módulo anti-fraude!';
$_['text_list']        = 'Lista anti-fraude';

// Column
$_['column_name']      = 'Nombre Anti-Fraude';
$_['column_status']    = 'Estado';
$_['column_action']    = 'Acción';

// Error
$_['error_permission'] = 'Advertencia: No tienes permiso para modificar el modulo anti-fraude!';