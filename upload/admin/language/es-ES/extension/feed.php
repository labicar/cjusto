<?php
// Heading
$_['heading_title']    = 'Fuentes';

// Text
$_['text_success']     = '¡Ha modificado correctamente los feeds!';
$_['text_list']        = 'Lista de Feed';

// Column
$_['column_name']      = 'Producto Nombre del Feed';
$_['column_status']    = 'Estado';
$_['column_action']    = 'Acción';

// Error
$_['error_permission'] = '¡Advertencia: No tienes permiso para modificar las fuentes!';