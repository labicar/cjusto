<?php
// Heading
$_['heading_title']         = 'Campos personalizados';

// Text
$_['text_success']          = 'Éxito: Ha modificado los campos personalizados!';
$_['text_list']             = 'Lista de campos personalizados';
$_['text_add']              = 'Añadir Campo Personalizado';
$_['text_edit']             = 'Editar campo personalizado';
$_['text_choose']           = 'Selección Única';
$_['text_select']           = 'Selección Desplegable';
$_['text_radio']            = 'Radio';
$_['text_checkbox']         = 'Checkbox';
$_['text_input']            = 'Input';
$_['text_text']             = 'Texto';
$_['text_textarea']         = 'Área de Texto';
$_['text_file']             = 'Archivo';
$_['text_date']             = 'Fecha';
$_['text_datetime']         = 'Fecha &amp; Hora';
$_['text_time']             = 'Hora';
$_['text_account']          = 'Cuenta';
$_['text_address']          = 'Dirección';

// Column
$_['column_name']           = 'Nombre de campo personalizado';
$_['column_location']       = 'Localización';
$_['column_type']           = 'Tipo';
$_['column_sort_order']     = 'Ordenar por';
$_['column_action']         = 'Acción';

// Entry
$_['entry_name']            = 'Nombre de campo personalizado';
$_['entry_location']        = 'Localización';
$_['entry_type']            = 'Tipo';
$_['entry_value']           = 'Valor';
$_['entry_custom_value']    = 'Nombre de valor del campo personalizado';
$_['entry_customer_group']  = 'Grupo de Clientes';
$_['entry_required']        = 'Requerido';
$_['entry_status']          = 'Estado';
$_['entry_sort_order']      = 'Ordenar por';

// Help
$_['help_sort_order']       = 'Utilice el signo menos para contar hacia atrás desde el último campo';

// Error
$_['error_permission']      = 'Advertencia: No tiene permisos para modificar los campos personalizados';
$_['error_name']            = 'Nombre del campo personalizado debe tener entre 1 y 128 caracteres.';
$_['error_type']            = 'Advertencia: Valores de campo personalizado requeridos.';
$_['error_custom_value']    = '¡El nombre del Valor de la Opción debe tener entre 1 y 128 carácteres!';