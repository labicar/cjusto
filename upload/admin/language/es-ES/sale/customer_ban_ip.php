<?php
// Heading
$_['heading_title']    = 'IPs bloqueadas';

// Text
$_['text_success']     = '¡Éxito: Ha modificado las IPs bloqueadas del clientes!';
$_['text_list']        = 'Lista de IP bloqueadas';
$_['text_add']         = 'Agregar IP bloqueada';
$_['text_edit']        = 'Editar IP bloqueada';

// Column
$_['column_ip']        = 'IP';
$_['column_customer']  = 'Clientes';
$_['column_action']    = 'Acción';

// Entry
$_['entry_ip']         = 'IP';

// Error
$_['error_permission'] = '¡Advertencia: No tiene permisos para modificar la IPs bloqueadas de clientes!';
$_['error_ip']         = '¡IP debe tener entre 1 y 40 caracteres!';