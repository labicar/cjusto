<?php
// Heading
$_['heading_title']       = 'Devolución de productos';

// Text
$_['text_success']        = '¡Ha modificado correctamente las devoluciones!';
$_['text_list']           = 'Lista de devolución de productos';
$_['text_add']            = 'Añadir devolución de producto';
$_['text_edit']           = 'Editar devolución de producto';
$_['text_opened']         = 'Abierto';
$_['text_unopened']       = 'Sin abrir';
$_['text_order']          = 'Información del pedido';
$_['text_product']        = 'Información de producto y motivo de devolución';
$_['text_history']        = 'Añadir historial de devolución';

// Column
$_['column_return_id']     = 'ID de Devolución';
$_['column_order_id']      = 'ID del Pedido';
$_['column_customer']      = 'Cliente';
$_['column_product']       = 'Producto';
$_['column_model']         = 'Modelo';
$_['column_status']        = 'Estado';
$_['column_date_added']    = 'Agregar Fecha';
$_['column_date_modified'] = 'Fecha Modificación';
$_['column_comment']       = 'Comentario';
$_['column_notify']        = 'Cliente notificado';
$_['column_action']        = 'Acción';

// Entry
$_['entry_customer']      = 'Cliente';
$_['entry_order_id']      = 'ID del Pedido';
$_['entry_date_ordered']  = 'Fecha de pedido';
$_['entry_firstname']     = 'Nombre';
$_['entry_lastname']      = 'Apellidos';
$_['entry_email']         = 'E-Mail';
$_['entry_telephone']     = 'Teléfono';
$_['entry_product']       = 'Producto';
$_['entry_model']         = 'Modelo';
$_['entry_quantity']      = 'Cantidad';
$_['entry_opened']        = 'Abierto';
$_['entry_comment']       = 'Comentario';
$_['entry_return_reason'] = 'Razón de devolución';
$_['entry_return_action'] = 'Acción de devolución';
$_['entry_return_status'] = 'Estado de la devolución';
$_['entry_notify']        = 'Notificar al cliente';
$_['entry_return_id']     = 'ID de Devolución';
$_['entry_date_added']    = 'Agregar Fecha';
$_['entry_date_modified'] = 'Fecha Modificación';

// Help
$_['help_product']        = '(Autocompletar)';

// Error
$_['error_warning']       = '¡Advertencia: Por favor revise cuidadosamente los errores en el formulario!';
$_['error_permission']    = '¡Advertencia: No tienes permiso para modificar las devoluciones!';
$_['error_order_id']      = '¡Se requiere un ID de orden!';
$_['error_firstname']     = 'El nombre debe tener entre 1 y 21 caracteres!';
$_['error_lastname']      = 'El apellido debe tener entre 1 y 21 caracteres!';
$_['error_email']         = 'La dirección de correo electrónico no parece ser válida!';
$_['error_telephone']     = '¡El teléfono debe tener entre 3 y 32 caracteres!';
$_['error_product']       = '¡El nombre del producto debe ser mayor que 3 y menor que 255 caracteres!';
$_['error_model']         = '¡El modelo del producto debe ser mayor que 3 y menor que 64 caracteres!';