<?php
// Heading
$_['heading_title']     = 'Vales de Regalo';

// Text
$_['text_success']      = 'Éxito: Ha modificado los cupones!';
$_['text_list']         = 'Lista de Vales de Regalo';
$_['text_add']          = 'Agregar Vales de Regalo';
$_['text_edit']         = 'Editar Vales de Regalo';
$_['text_sent']         = 'Éxito: El Vale de Regalo ha sido enviado por correo electrónico!';

// Column
$_['column_name']       = 'Nombre delcupon';
$_['column_code']       = 'Código';
$_['column_from']       = 'De';
$_['column_to']         = 'Para';
$_['column_theme']      = 'Plantilla';
$_['column_amount']     = 'Importe';
$_['column_status']     = 'Estado';
$_['column_order_id']   = 'ID del Pedido';
$_['column_customer']   = 'Cliente';
$_['column_date_added'] = 'Agregar Fecha';
$_['column_action']     = 'Acción';

// Entry
$_['entry_code']        = 'Código';
$_['entry_from_name']   = 'De nombre';
$_['entry_from_email']  = 'Desde el correo electrónico';
$_['entry_to_name']     = 'Nombre Para';
$_['entry_to_email']    = 'Al correo electrónico';
$_['entry_theme']       = 'Plantilla';
$_['entry_message']     = 'Mensaje';
$_['entry_amount']      = 'Importe';
$_['entry_status']      = 'Estado';

// Help
$_['help_code']         = 'Ingresa el código del cliente para activar el Vale.';

// Error
$_['error_selection']   = 'ADVERTENCIA: No hay vales seleccionados!';
$_['error_permission']  = 'ADVERTENCIA: No tienes permiso para modificar los Vales!';
$_['error_exists']      = 'ADVERTENCIA: El código del vale ya está en uso!';
$_['error_code']        = 'El código debe ser entre 3 y 10 caracteres!';
$_['error_to_name']     = 'El nombre del destinatario debe contener entre 1 y 64 caracteres!';
$_['error_from_name']   = 'Tu nombre debe contener entre 1 y 64 caracteres!';
$_['error_email']       = 'La dirección de correo electrónico no parece ser válida!';
$_['error_amount']      = 'Cantidad debe ser mayor o igual a 1!';
$_['error_order']       = 'ADVERTENCIA: Este Vale no puede ser borrado como parte de una <a href="%s"> orden</a>!';