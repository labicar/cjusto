<?php
// header
$_['heading_title']   = '¿Olvidó su contraseña?';

// Text
$_['text_forgotten']  = 'Contraseña olvidada';
$_['text_your_email'] = 'Su dirección de correo electrónico';
$_['text_email']      = 'Introduzca la dirección de correo electrónico asociada a su cuenta. Haga clic en enviar para recibir un enlace por correo electrónico para restaurar la contraseña.';
$_['text_success']    = 'Un correo electrónico con un enlace de confirmación ha sido enviado a su dirección de correo electrónico de administrador.';

// Entry
$_['entry_email']     = 'Dirección de correo electrónico';
$_['entry_password']  = 'Nueva Contraseña';
$_['entry_confirm']   = 'Confirmar';

// Error
$_['error_email']     = 'ADVERTENCIA: La dirección de correo electrónico no fue encontrada en nuestros registros, por favor intentelo de nuevo!';
$_['error_password']  = 'La contraseña debe tener entre 3 y 20 caracteres!';
$_['error_confirm']   = 'La contraseña y su confirmación no coinciden!';