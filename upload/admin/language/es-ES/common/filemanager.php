<?php
// Heading
$_['heading_title']    = 'Administrador de imágenes';

// Text
$_['text_uploaded']    = 'El archivo ha sido subido satisfactoriamente!';
$_['text_directory']   = 'Directorio creado satisfactoriamente!';
$_['text_delete']      = 'El archivo o directorio ha sido eliminado satisfactoriamente!';

// Entry
$_['entry_search']     = 'Buscar..';
$_['entry_folder']     = 'Nombre de la carpeta';

// Error
$_['error_permission'] = 'ADVERTENCIA: Permiso denegado!';
$_['error_filename']   = 'ADVERTENCIA: El nombre del archivo debe tener entre 3 y 255 caracteres!';
$_['error_folder']     = 'ADVERTENCIA: El nombre de la carpeta debe tener entre 3 y 255 caracteres!';
$_['error_exists']     = 'ADVERTENCIA: Un archivo o directorio con el mismo nombre ya existe!';
$_['error_directory']  = 'ADVERTENCIA: El directorio no existe!';
$_['error_filetype']   = 'ADVERTENCIA: El tipo de archivo es incorrecto!';
$_['error_upload']     = 'ADVERTENCIA: El archivo no pudo ser cargado por razones desconocidas!';
$_['error_delete']     = 'ADVERTENCIA: No puede borrar este directorio!';