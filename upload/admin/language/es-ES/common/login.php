<?php
// header
$_['heading_title']  = 'Administración';

// Text
$_['text_heading']   = 'Administración';
$_['text_login']     = 'Por favor ingrese sus datos de inicio de sesión.';
$_['text_forgotten'] = 'Contraseña olvidada';

// Entry
$_['entry_username'] = 'Usuario';
$_['entry_password'] = 'Contraseña';

// Button
$_['button_login']   = 'Acceder';

// Error
$_['error_login']    = 'El usuario y/o contraseña no coinciden.';
$_['error_token']    = 'Sesión de token no válido. Por favor ingresa nuevamente.';