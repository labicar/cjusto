<?php
// Heading
$_['heading_title']    = 'Botón de PayPal Express Checkout';

// Text
$_['text_module']      = 'Módulos';
$_['text_success']     = 'Éxito: Ha modificado módulo de PayPal Express Checkout botón!';
$_['text_edit']        = 'Editar el botón PayPal Express Checkout';

// Entry
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'ADVERTENCIA: No tienes permiso para modificar el módulo de PayPal Express Checkout botón!';