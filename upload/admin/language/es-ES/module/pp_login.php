<?php
// Heading
$_['heading_title']        = 'Inicie la sesión con PayPal';

//Text
$_['text_module']          = 'Módulos';
$_['text_success']         = 'Éxito: Ha modificado el módulo Log In de PayPal!';
$_['text_edit']            = 'Editar Registro con módulo de PayPal';
$_['text_button_grey']     = 'Gris';
$_['text_button_blue']     = 'Azul (recomendado)';

//Entry
$_['entry_client_id']      = 'Client ID';
$_['entry_secret']         = 'Secreto';
$_['entry_sandbox']        = 'Modo Sandbox';
$_['entry_debug']          = 'Depurar registro';
$_['entry_customer_group'] = 'Grupo de Clientes';
$_['entry_button']         = 'Color del botón';
$_['entry_seamless']       = 'Permitir "Seamless Checkout"';
$_['entry_locale']         = 'Localidad';
$_['entry_return_url']     = 'URL devuelta';
$_['entry_status']         = 'Estado';

//Help
$_['help_sandbox']         = '¿Utilizar entorno sandbox (pruebas)?';
$_['help_customer_group']  = '¿Para los nuevos clientes, en qué Grupo de Clientes se deben crear?';
$_['help_debug_logging']   = 'Habilitar esto permitirá que se añadan datos al registro de errores para ayudar a depurar cualquier problema.';
$_['help_seamless']        = 'Permite auto-login cuando los clientes eligen PayPal Express Checkout. Para utilizar esto, debe estar habilitada la opción en su registro con cuenta de PayPal. También se debe usar la misma cuenta que el utilizado en Express Checkout.';
$_['help_locale']          = 'Esta es la configuración regional de PayPal para tus idiomas de la tienda';
$_['help_return_url']      = 'Debe añadirse en la configuración de la aplicación de PayPal en el app de redirecciones de URL.';

//Error
$_['error_permission']     = 'ADVERTENCIA: ¡No tienes permiso para modificar el Acceso en el módulo de PayPal!';
$_['error_client_id']      = '¡ID de cliente requerido!';
$_['error_secret']         = '¡\'Secret\' requerido!';