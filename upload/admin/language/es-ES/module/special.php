<?php
// Heading
$_['heading_title']    = 'Promociones especiales';

// Text
$_['text_module']      = 'Módulos';
$_['text_success']     = '¡Ha modificado módulo \'Promociones especiales\' con éxito!';
$_['text_edit']        = 'Editar módulo \'Promociones especiales\'';

// Entry
$_['entry_name']       = 'Nombre del Módulo';
$_['entry_limit']      = 'Límite';
$_['entry_width']      = 'Ancho';
$_['entry_height']     = 'Alto';
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'ADVERTENCIA: ¡No tienes permiso para modificar el módulo \'Promociones especiales\'!';
$_['error_name']       = '¡El nombre del módulo debe tener entre 3 y 64 caracteres!';
$_['error_width']      = '¡Anchura requerida!';
$_['error_height']     = '¡Altura requerida!';