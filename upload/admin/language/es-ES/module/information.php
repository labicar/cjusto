<?php
// Heading
$_['heading_title']    = 'Información';

// Text
$_['text_module']      = 'Módulos';
$_['text_success']     = 'Has modificado el módulo de información con éxito!';
$_['text_edit']        = 'Editar módulo de información';

// Entry
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'ATENCION: No tienes permisos para modificar el módulo de información!';