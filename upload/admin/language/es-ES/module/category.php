<?php
// Heading
$_['heading_title']    = 'Categoría';

// Text
$_['text_module']      = 'Módulos';
$_['text_success']     = 'Éxito: Ha modificado módulo de categoría!';
$_['text_edit']        = 'Editar el módulo Categoría';

// Entry
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: No tienes permiso para modificar el módulo de categoría!';