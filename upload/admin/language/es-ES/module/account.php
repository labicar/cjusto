<?php
// Heading
$_['heading_title']    = 'Cuenta';

$_['text_module']      = 'Módulos';
$_['text_success']     = 'Éxito: Ha modificado el módulo de cuenta!';
$_['text_edit']        = 'Editar el módulo de cuenta';

// Entry
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: No tiene permiso para modificar el módulo de cuenta!';