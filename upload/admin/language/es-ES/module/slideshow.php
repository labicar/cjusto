<?php
// Heading
$_['heading_title']    = 'Presentación de diapositivas';

// Text
$_['text_module']      = 'Módulos';
$_['text_success']     = 'Éxito: ¡Ha modificado el módulo de diapositivas!';
$_['text_edit']        = 'Editar módulo Diapositivas';

// Entry
$_['entry_name']       = 'Nombre del Módulo';
$_['entry_banner']     = 'Banner';
$_['entry_width']      = 'Ancho';
$_['entry_height']     = 'Alto';
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'ATENCION: ¡No tienes permisos para modificar el módulo Diapositivas!';
$_['error_name']       = '¡El nombre del módulo debe tener entre 3 y 64 caracteres!';
$_['error_width']      = '¡Anchura requerida!';
$_['error_height']     = '¡Altura requerida!';