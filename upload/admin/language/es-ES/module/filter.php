<?php
// Heading
$_['heading_title']    = 'Filtrar';

// Text
$_['text_module']      = 'Módulos';
$_['text_success']     = 'Éxito: Ha modificado el módulo del filtro!';
$_['text_edit']        = 'Editar módulo de filtro';

// Entry
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: No tienes permiso para modificar el módulo del filtro!';