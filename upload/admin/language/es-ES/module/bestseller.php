<?php
// Heading
$_['heading_title']    = 'Más vendidos';

// Text
$_['text_module']      = 'Módulos';
$_['text_success']     = 'Éxito: Ha modificado módulo de Más Vendidos!';
$_['text_edit']        = 'Editar el módulo de Best-sellers';

// Entry
$_['entry_name']       = 'Nombre del Módulo';
$_['entry_limit']      = 'Límite';
$_['entry_image']      = 'Imagen (W x H) y cambiar el tipo de tamaño';
$_['entry_width']      = 'Ancho';
$_['entry_height']     = 'Alto';
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: No tienes permiso para modificar el módulo de Más Vendidos!';
$_['error_name']       = '¡El nombre del módulo debe tener entre 3 y 64 caracteres!';
$_['error_width']      = '¡Anchura requerida!';
$_['error_height']     = '¡Altura requerida!';