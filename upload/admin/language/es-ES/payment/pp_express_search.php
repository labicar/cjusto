<?php
// Heading
$_['heading_title']					= 'Buscar Operaciones';

// Column
$_['tbl_column_date']				= 'Fecha';
$_['tbl_column_type']				= 'Tipo';
$_['tbl_column_email']				= 'Correo electrónico';
$_['tbl_column_name']				= 'Nombre';
$_['tbl_column_transid']			= 'ID de la Transacción';
$_['tbl_column_status']				= 'Estado';
$_['tbl_column_currency']			= 'Moneda';
$_['tbl_column_amount']				= 'Importe';
$_['tbl_column_fee']				= 'Tarifa';
$_['tbl_column_netamt']				= 'Importe neto';
$_['tbl_column_action']				= 'Acción';

// Text
$_['text_pp_express']				= 'PayPal Express Checkout';
$_['text_date_search']				= 'Buscar por fecha';
$_['text_searching']				= 'Buscando';
$_['text_name']						= 'Nombre';
$_['text_buyer_info']				= 'Información del comprador';
$_['text_view']						= 'Ver';
$_['text_format']					= 'Formato';

// Entry
$_['entry_trans_all']				= 'Todo';
$_['entry_trans_sent']				= 'Enviado';
$_['entry_trans_received']			= 'Recibido';
$_['entry_trans_masspay']			= 'Pago total';
$_['entry_trans_money_req']			= 'Solicitud de dinero';
$_['entry_trans_funds_add']			= 'Fondos añadidos';
$_['entry_trans_funds_with']		= 'Retirada de fondos';
$_['entry_trans_referral']			= 'Referencia';
$_['entry_trans_fee']				= 'Tarifa';
$_['entry_trans_subscription']		= 'Suscripción';
$_['entry_trans_dividend']			= 'Dividendo';
$_['entry_trans_billpay']			= 'Pago de facturas';
$_['entry_trans_refund']			= 'Reembolso';
$_['entry_trans_conv']				= 'Conversión de moneda';
$_['entry_trans_bal_trans']			= 'Balance Transfer';
$_['entry_trans_reversal']			= 'Anulación';
$_['entry_trans_shipping']			= 'Envío';
$_['entry_trans_bal_affect']		= 'Balance Affecting';
$_['entry_trans_echeque']			= 'Cheque electrónico';
$_['entry_date']					= 'Fecha';
$_['entry_date_start']				= 'Comienzar';
$_['entry_date_end']				= 'Fin';
$_['entry_date_to']					= 'para';
$_['entry_transaction']				= 'Transacción';
$_['entry_transaction_type']		= 'Tipo';
$_['entry_transaction_status']		= 'Estado';
$_['entry_email']					= 'Correo electrónico';
$_['entry_email_buyer']				= 'Comprador';
$_['entry_email_merchant']			= 'Receptor';
$_['entry_receipt']					= 'Receipt ID';
$_['entry_transaction_id']			= 'ID de la Transacción';
$_['entry_invoice_no']				= 'Invoice number';
$_['entry_auction']					= 'Auction item number';
$_['entry_amount']					= 'Importe';
$_['entry_recurring_id']			= 'Recurring Profile ID';
$_['entry_salutation']				= 'Salutation';
$_['entry_firstname']				= 'Primero 	';
$_['entry_middlename']				= 'Centrado';
$_['entry_lastname']				= 'Ultimo';
$_['entry_suffix']					= 'Suffix';
$_['entry_status_all']				= 'Todo';
$_['entry_status_pending']			= 'pendiente';
$_['entry_status_processing']		= 'Procesando';
$_['entry_status_success']			= 'Éxito';
$_['entry_status_denied']			= 'Denegado';
$_['entry_status_reversed']			= 'Invertido';