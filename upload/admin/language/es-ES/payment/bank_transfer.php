<?php
// Heading
$_['heading_title']					= 'Transferencia bancaria';

// Text
$_['text_payment']					= 'Pago';
$_['text_success']					= '¡Éxito: Ha modificado los detalles de transferencia bancaria!';
$_['text_edit']                     = 'Editar Transferencia Bancaria';

// Entry
$_['entry_bank']					= 'Instrucciones de transferencia bancaria';
$_['entry_total']					= 'Total ';
$_['entry_order_status']			= 'Estado del Pedido';
$_['entry_geo_zone']				= 'Zona geográfica';
$_['entry_status']					= 'Estado';
$_['entry_sort_order']				= 'Ordenar por';

// Help
$_['help_total']					= 'El total a pagar que la órden debe alcanzar para que este método se active.';

// Error
$_['error_permission']				= '¡Advertencia: No tiene permisos para modificar le pago con transferencia bancaria!';
$_['error_bank']					= '¡Instrucciones de transferencia bancaria requeridas!';