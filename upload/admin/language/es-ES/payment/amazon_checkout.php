<?php
// Heading
$_['heading_title']					= 'Pago por amazon';

// Text
$_['text_payment']					= 'Pago';
$_['text_success']					= 'Éxito: Pago por modulo de Amazon ha sido actualizado';
$_['text_edit']                     = 'Editar Amazon Payment';
$_['text_germany']					= 'Alemania';
$_['text_uk']						= 'Reino Unido';
$_['text_live']						= 'En vivo';
$_['text_sandbox']					= 'Sandbox';
$_['text_upload_success']			= 'Archivo cargado con éxito';
$_['text_button_settings']			= 'Configuración de botón de pago';
$_['text_orange']					= 'Naranja';
$_['text_tan']						= 'Tan';
$_['text_white']					= 'Blanco';
$_['text_light']					= 'Ligero';
$_['text_dark']						= 'Oscuro';
$_['text_medium']					= 'Mediano';
$_['text_large']					= 'Grande';
$_['text_x_large']					= 'Extra grande';
$_['text_download']					= '<a href="%s" target="_blank"> <u>Descargar</u></a> archivo de plantilla de Amazon Seller Central.';
$_['text_amazon_details']			= 'Detalles de Amazon';
$_['text_amazon_order_id']			= 'ID de pedido de Amazon';
$_['text_upload']					= 'Subir';
$_['text_upload_template']			= 'Subir la plantilla haciendo clic en el botón de abajo. Asegúrese de que está guardado como un archivo delimitado por tabuladores.';
$_['text_amazon_checkout']			= '<a onclick="window.open(\'http://go.amazonservices.com/UKCBASPOpenCart.html\');"><img src="view/image/payment/amazon.png" alt="Checkout by Amazon" title="Checkout by Amazon" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_amazon_join']				= 'Para crear su cuenta de pagos por Amazon <a href="http://go.amazonservices.com/UKCBASPOpenCart.html" target="_blank" title="Click here to join Checkout by Amazon" class="alert-link">haga clic aquí.</a>';

// Column
$_['column_submission_id']			= 'ID del envío';
$_['column_status']					= 'Estado';
$_['column_text']					= 'Respuesta';
$_['column_amazon_order_item_code'] = 'Código de producto de pedido de Amazon';

// Entry
$_['entry_merchant_id']				= 'ID comerciante';
$_['entry_access_key']				= 'Tecla de acceso';
$_['entry_access_secret']			= 'Clave secreta';
$_['entry_checkout_mode']			= 'Modo de pago';
$_['entry_marketplace']				= 'Marketplace';
$_['entry_order_status']			= 'Estado del pedido';
$_['entry_ready_status']		    = 'Listo para ser enviado';
$_['entry_shipped_status']			= 'Orden enviada';
$_['entry_canceled_status']			= 'Orden cancelada';
$_['entry_cron_job_url']			= 'URL de cronjob';
$_['entry_cron_job_token']			= 'Token secreto';
$_['entry_cron_job_last_run']		= 'Última hora de ejecución del trabajo de Cron';
$_['entry_ip']						= 'Dirección IP';
$_['entry_ip_allowed']				= 'IPs permitidas';
$_['entry_total']	         		= 'Total ';
$_['entry_geo_zone']			    = 'Zona geográfica';
$_['entry_status']					= 'Estado';
$_['entry_sort_order']				= 'Ordenar por';
$_['entry_colour']					= 'Color';
$_['entry_background']				= 'Fondo';
$_['entry_size']				    = 'Tamaño';

// Help
$_['help_cron_job_url']				= 'Configurar una tarea programada para llamar a esta URL';
$_['help_adjustment']				= 'Se requieren campos en negrita y al menos un campo "-adj".';
$_['help_allowed_ips']				= 'Deje en blanco si desea que todos vean el botón pagar';
$_['help_ip']						= 'e.g 203.0.113.0 <br />Dejar vació para permitir cualquier IP.';
$_['help_cron_job_token']			= 'Hacer esto largo y difícil de adivinar';

// Tab
$_['tab_order_adjustment']			= 'Ajustes de pedido';

// Error
$_['error_permissions']				= 'No tiene permisos para modificar este módulo';
$_['error_access_secret']		    = 'Clave secreta es requerida';
$_['error_access_key']		        = 'Se requiere una clave de acceso';
$_['error_merchant_id']		        = 'El ID de comerciante es requerido';
$_['error_curreny']					= 'Tu tienda debe tener%s de moneda instalado y habilitado';
$_['error_upload']					= 'Fallo en la subida';