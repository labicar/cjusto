<?php
// Heading
$_['heading_title']			= 'Authorize.Net (SIM)';

// Text
$_['text_payment']			= 'Pagar';
$_['text_success']			= 'Éxito: ¡ha modificado los detalles de la cuenta de Authorize.Net (SIM)!';
$_['text_edit']             = 'Editar Authorize.Net (SIM)';
$_['text_authorizenet_sim']	= '<a onclick="window.open(\'http://reseller.authorize.net/application/?id=5561142\');"><img src="view/image/payment/authorizenet.png" alt="Authorize.Net" title="Authorize.Net" style="border: 1px solid #EEEEEE;" /></a>';

// Entry
$_['entry_merchant']		= 'ID comerciante';
$_['entry_key']				= 'Clave de transacción';
$_['entry_callback']		= 'URL de respuesta de relevo';
$_['entry_md5']				= 'Valor de Hash MD5';
$_['entry_test']			= 'Modo de prueba';
$_['entry_total']			= 'Total ';
$_['entry_order_status']	= 'Estado del pedido';
$_['entry_geo_zone']		= 'Zona geográfica';
$_['entry_status']			= 'Estado';
$_['entry_sort_order']		= 'Ordenar por';

// Help
$_['help_callback']			= 'Por favor inicie sesión y configure esto en <a href="https://secure.authorize.net" target="_blank" class="txtLink">https:/secure.authorize.net</a>.';
$_['help_md5']				= 'La función MD5 Hash le permite autenticar que una respuesta de la transacción es bien recibida de Authorize.Net.Please inicie sesión y configure esto a <a href="https://secure.authorize.net" target="_blank" class="txtLink">https://secure.authorize.net</a>. (Opcional)';
$_['help_total']			= 'El total a pagar que el pedido debe alcanzar para que este método se active.';

// Error
$_['error_permission']		= 'Advertencia: ¡No tienes permiso para modificar el pago Authorize.Net (SIM)!';
$_['error_merchant']		= '¡ID comerciante requerida!';
$_['error_key']				= '¡Clave de transacción requerida!';