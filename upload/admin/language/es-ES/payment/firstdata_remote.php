<?php
// Heading
$_['heading_title']					 = 'First Data EMEA Web Service API';

// Text
$_['text_firstdata_remote']			 = '<img src="view/image/payment/firstdata.png" alt="First Data" title="First Data" style="border: 1px solid #EEEEEE;" />';
$_['text_payment']					 = 'Pago';
$_['text_success']					 = 'Éxito: ¡Ha modificado los detalles de su cuenta de Primeros Datos!';
$_['text_edit']                      = 'Edit First Data EMEA Web Service API';
$_['text_card_type']				 = 'Tipo de tarjeta';
$_['text_enabled']					 = 'Activado';
$_['text_merchant_id']				 = 'ID Tienda';
$_['text_subaccount']				 = 'Subcuenta';
$_['text_user_id']					 = 'ID de usuario';
$_['text_capture_ok']				 = 'La captura fue exitosa';
$_['text_capture_ok_order']			 = 'La captura fue exitosa, estado de pedido se actualizo y se estableció con éxito';
$_['text_refund_ok']				 = 'El reembolso fue exitoso';
$_['text_refund_ok_order']			 = 'El reembolso fue exitoso, estado de pedido actualizado a reembolso';
$_['text_void_ok']					 = 'La anulación fue exitosa, el estado de pedido actualizado a anulado';
$_['text_settle_auto']				 = 'Rebajas';
$_['text_settle_delayed']			 = 'Pre autenticación';
$_['text_mastercard']				 = 'Mastercard';
$_['text_visa']						 = 'Visa';
$_['text_diners']					 = 'Diners';
$_['text_amex']						 = 'American Express';
$_['text_maestro']					 = 'Maestro';
$_['text_payment_info']				 = 'Información de pago';
$_['text_capture_status']			 = 'Pago capturado';
$_['text_void_status']				 = 'Pago anulado';
$_['text_refund_status']			 = 'Pago reembolsado';
$_['text_order_ref']				 = 'Ref. de pedido';
$_['text_order_total']				 = 'Total autorizado';
$_['text_total_captured']			 = 'Total capturado';
$_['text_transactions']				 = 'Transacciones';
$_['text_column_amount']			 = 'Importe';
$_['text_column_type']				 = 'Tipo';
$_['text_column_date_added']		 = 'Creado';
$_['text_confirm_void']				 = '¿Está seguro de que quiere invalidar el pago?';
$_['text_confirm_capture']			 = '¿Está seguro que desea capturar el pago?';
$_['text_confirm_refund']			 = '¿Está seguro que desea reembolso el pago?';

// Entry
$_['entry_certificate_path']		 = 'Ruta del certificado';
$_['entry_certificate_key_path']	 = 'Ruta de clave privada';
$_['entry_certificate_key_pw']		 = 'Contraseña de clave privada';
$_['entry_certificate_ca_path']		 = 'Ruta CA';
$_['entry_merchant_id']				 = 'ID Tienda';
$_['entry_user_id']					 = 'ID de usuario';
$_['entry_password']				 = 'Contraseña';
$_['entry_total']					 = 'Total ';
$_['entry_sort_order']				 = 'Ordenar por';
$_['entry_geo_zone']				 = 'Zona geográfica';
$_['entry_status']					 = 'Estado';
$_['entry_debug']					 = 'Depurar registro';
$_['entry_auto_settle']				 = 'Tipo de liquidación';
$_['entry_status_success_settled']	 = 'Éxito - arreglado';
$_['entry_status_success_unsettled'] = 'Éxito - No arreglado';
$_['entry_status_decline']			 = 'Rechazar';
$_['entry_status_void']				 = 'Anuladas';
$_['entry_status_refund']			 = 'Reembolsado';
$_['entry_enable_card_store']		 = 'Habilitar fichas de almacenamiento de tarjetas';
$_['entry_cards_accepted']			 = 'Tipos de tarjeta aceptadas';

// Help
$_['help_total']					 = 'El total a pagar que el pedido debe alcanzar para que este método se active';
$_['help_certificate']				 = 'Los certificados y las claves privadas se deben almacenar fuera de las carpetas web públicas';
$_['help_card_select']				 = 'Pídale al usuario que elija su tipo de tarjeta antes de que sean redirigidos';
$_['help_notification']				 = 'Debe proporcionar esta URL a los Primeros Datos para recibir las notificaciones de pago';
$_['help_debug']					 = 'Debug permite escribir datos en un archivo de registro. Siempre deberá estar desactivado si no se indica lo contrario.';
$_['help_settle']					 = 'Si usa la pre-autorización, debe completar una acción de post-autenticación en un plazo de 3 a 5 días, de lo contrario su transacción de cancelará';

// Tab
$_['tab_account']					 = 'Información de la API';
$_['tab_order_status']				 = 'Estado del pedido';
$_['tab_payment']					 = 'Opciones de pago';

// Button
$_['button_capture']				= 'Captura';
$_['button_refund']					= 'Reembolso';
$_['button_void']					= 'Vacío';

// Error
$_['error_merchant_id']				= 'Se requiere ID de la tienda';
$_['error_user_id']					= 'Se necesita el ID del usuario';
$_['error_password']				= 'Se requiere una contraseña';
$_['error_certificate']				= 'Se requiere una ruta certificada';
$_['error_key']						= 'Se requiere clave de certificado';
$_['error_key_pw']					= 'Se requiere contraseña de la clave del certificado';
$_['error_ca']						= 'Se requiere una autoridad de certificación (CA)';