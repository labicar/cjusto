<?php
// Heading
$_['heading_title']				 = 'Pagos Online de Worldplay';

// Text
$_['text_payment']				 = 'Pagar';
$_['text_success']				 = 'Exito: ¡Ha modificado detalles de cuenta Worldpay!';
$_['text_worldpay']				 = '<img src="view/image/payment/worldpay.png" alt="Worldpay" title="Worldpay" style="border: 1px solid #EEEEEE;" />';
$_['text_test']					 = 'Test';
$_['text_live']					 = 'En vivo';
$_['text_authenticate']			 = 'Autentificar';
$_['text_release_ok']		 	 = 'Enviado con éxito';
$_['text_release_ok_order']		 = 'Lanzamiento exitoso, orden de estatus actualizado - liquidado';
$_['text_refund_ok']			 = 'El reemolso fue exitoso';
$_['text_refund_ok_order']		 = 'Reembolso exitoso, orden de estatus actualizada a reembolso';
$_['text_void_ok']				 = 'La anulación fue exitosa, orden de estatus actualizado a anulado';

// Entry
$_['entry_service_key']			 = 'Clave de servicio';
$_['entry_client_key']			 = 'Clave de cliente';
$_['entry_total']				 = 'Total ';
$_['entry_order_status']		 = 'Estado del pedido';
$_['entry_geo_zone']			 = 'Zona geográfica';
$_['entry_status']				 = 'Estado';
$_['entry_sort_order']			 = 'Ordenar por';
$_['entry_debug']				 = 'Depurar registro';
$_['entry_card']				 = 'Tarjetas de tiendas';
$_['entry_secret_token']		 = 'Token secreto';
$_['entry_webhook_url']			 = 'Webhook URL:';
$_['entry_cron_job_url']		 = 'Trabajo Cron URL:';
$_['entry_last_cron_job_run']	 = 'Ultimo tiempo de ejecución de trabajos cron:';
$_['entry_success_status']		 = 'Estatus Exitoso:';
$_['entry_failed_status']		 = 'Estatus fallido:';
$_['entry_settled_status']			= 'Estatus liquidado:';
$_['entry_refunded_status']			= 'Estatus reembolsado:';
$_['entry_partially_refunded_status']	= 'Estatus Parcialmente Reembolsado:';
$_['entry_charged_back_status']			= 'Cargado devuelta:';
$_['entry_information_requested_status']			= 'Estatus de Información solicitada:';
$_['entry_information_supplied_status']			= 'Estatus de Información Suministrada:';
$_['entry_chargeback_reversed_status']			= 'Estatus de Devolución Reversa:';


$_['entry_reversed_status']			= 'Estatus Revertido:';
$_['entry_voided_status']			= 'Estatus Anulado:';

// Help
$_['help_total']				 = 'El total a pagar que el pedido debe alcanzar para que este método se active.';
$_['help_debug']				 = 'Habilitar depuración escribirá datos sensibles en el registro de archivos. Debería deshabilitar siempre a menos que se le indique lo contrario';
$_['help_secret_token']			 = 'Hacer esto largo y difícil de adivinar';
$_['help_webhook_url']			 = 'Establecer webhooks de Worldpay para llamar este URL';
$_['help_cron_job_url']			 = 'Configurar una tarea programada para llamar a esta URL';

// Tab
$_['tab_settings']				 = 'Configuración';
$_['tab_order_status']			 = 'Estado del pedido';

// Error
$_['error_permission']			 = 'Advertencia: ¡No tiene permiso de modificar el pago de Worldpay!';
$_['error_service_key']			 = '¡clave de servicio requerida!';
$_['error_client_key']			 = '¡Clave de cliente Requerida!';

// Order page - payment tab
$_['text_payment_info']			 = 'Información de pago';
$_['text_refund_status']		 = 'Reembolso de pago';
$_['text_order_ref']			 = 'Ref. de pedido';
$_['text_order_total']			 = 'Total autorizado';
$_['text_total_released']		 = 'Total liberado';
$_['text_transactions']			 = 'Transacciones';
$_['text_column_amount']		 = 'Importe';
$_['text_column_type']			 = 'Tipo';
$_['text_column_date_added']	 = 'Agregado';

$_['text_confirm_refund']		 = '¿Está seguro que desea reembolso el pago?';

$_['btn_refund']				 = 'Reembolso / devolución';

