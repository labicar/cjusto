<?php
// Heading
$_['heading_title']		 = 'LIQPAY';

// Text
$_['text_payment']		 = 'Pago';
$_['text_success']		 = 'Success: You have modified LIQPAY account details!';
$_['text_edit']          = 'Editar LIQPAY';
$_['text_pay']			 = 'LIQPAY';
$_['text_card']			 = 'Tarjeta de crédito';
$_['text_liqpay']		 = '<img src="view/image/payment/liqpay.png" alt="LIQPAY" title="LIQPAY" style="border: 1px solid #EEEEEE;" />';

// Entry
$_['entry_merchant']	 = 'ID comerciante';
$_['entry_signature']	 = 'Firma';
$_['entry_type']		 = 'Tipo';
$_['entry_total']		 = 'Total ';
$_['entry_order_status'] = 'Estado del pedido';
$_['entry_geo_zone']	 = 'Zona geográfica';
$_['entry_status']		 = 'Estado';
$_['entry_sort_order']	 = 'Ordenar por';

// Help
$_['help_total']		 = 'El total a pagar que el pedido debe alcanzar para que este método se active.';

// Error
$_['error_permission']	 = 'ADVERTENCIA: ¡No tienes permiso para modificar el pago PayPal!';
$_['error_merchant']	 = '¡ID comerciante requerida!';
$_['error_signature']	 = 'Firma requerida!';