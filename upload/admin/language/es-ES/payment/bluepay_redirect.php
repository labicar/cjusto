<?php
// Heading
$_['heading_title']					= 'BluePay Redirect (Requiere SSL)';

// Text
$_['text_payment']					= 'Pagar';
$_['text_success']					= '¡Éxito: Ha modificado los detalles de la cuenta de BluePay Redirect!';
$_['text_edit']                     = 'Editar BluerPay Redirect (requiere SSL)';
$_['text_bluepay_redirect']			= '<a href="http://www.bluepay.com/preferred-partner/opencart" target="_blank"><img src="view/image/payment/bluepay.jpg" alt="BluePay Redirect" title="BluePay Redirect" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_sim']						= 'Simulador';
$_['text_test']						= 'Test';
$_['text_live']						= 'En vivo';
$_['text_sale']					    = 'Rebajas';
$_['text_authenticate']				= 'Autentificar';
$_['text_release_ok']				= 'Enviado con éxito';
$_['text_release_ok_order']			= 'Enviado con éxito';
$_['text_rebate_ok']				= 'El reemolso fue exitoso';
$_['text_rebate_ok_order']			= 'El reembolso fue exitoso, orden de estatus actualizado a reembolso';
$_['text_void_ok']					= 'La anulación fue exitosa, orden de estatus actualizado a anulado';
$_['text_payment_info']				= 'Información de pago';
$_['text_release_status']			= 'Pago Liberado';
$_['text_void_status']				= 'Pago anulado';
$_['text_rebate_status']			= 'Pago reembolsado';
$_['text_order_ref']				= 'Ref. de pedido';
$_['text_order_total']				= 'Total autorizado';
$_['text_total_released']			= 'Total liberado';
$_['text_transactions']				= 'Transacciones';
$_['text_column_amount']			= 'Importe';
$_['text_column_type']				= 'Tipo';
$_['text_column_date_added']		= 'Creado';
$_['text_confirm_void']				= '¿Está seguro de que quiere invalidar el pago?';
$_['text_confirm_release']			= '¿Está seguro que desea reembolso el pago?';
$_['text_confirm_rebate']			= '¿Está seguro que desea reembolso el pago?';

// Entry
$_['entry_vendor']					= 'Identificación de la cuenta';
$_['entry_secret_key']				= 'Clave secreta';
$_['entry_test']					= 'Modo de prueba';
$_['entry_transaction']				= 'Método de transacción';
$_['entry_total']					= 'Total ';
$_['entry_order_status']			= 'Estado del pedido';
$_['entry_geo_zone']				= 'Zona geográfica';
$_['entry_status']					= 'Estado';
$_['entry_sort_order']				= 'Ordenar por';
$_['entry_debug']					= 'Depurar registro';
$_['entry_card']					= 'Tarjetas de tiendas';

// Help
$_['help_total']					= 'El total a pagar que el pedido debe alcanzar para que este método se active.';
$_['help_debug']					= 'Habilitar depuración escribirá datos sensibles en el registro de archivos. Usted debería siempre desactivar a menos que se indique lo contrario';
$_['help_transaction']				= 'Método de transacción DEBE establecerse en pago para permitir pagos a suscripción';
$_['help_cron_job_token']			= 'Hacer esto largo y difícil de adivinar';
$_['help_cron_job_url']				= 'Configurar una tarea programada para llamar a esta URL';

// Button
$_['btn_release']					= 'Lanzamiento';
$_['btn_rebate']					= 'Reembolso / devolución';
$_['btn_void']						= 'Vacío';

// Error
$_['error_permission']				= 'Advertencia: ¡No tiene permiso de modificar el pago de BluePay!';
$_['error_account_id']				= '¡ID de cuenta requerido!';
$_['error_secret_key']				= '¡Clave secreta necesaria!';