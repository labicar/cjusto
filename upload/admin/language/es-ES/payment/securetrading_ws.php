<?php
$_['heading_title'] = 'Secure Trading Web Service';

$_['tab_settings'] = 'Configuración';
$_['tab_myst'] = 'MyST';

$_['text_securetrading_ws'] = '<img src="view/image/payment/secure_trading.png" alt="Secure Trading" title="Secure Trading" style="border: 1px solid #EEEEEE;" />';
$_['text_payment'] = 'Pagar';
$_['text_all_geo_zones'] = 'Todas las Geo zonas';
$_['text_process_immediately'] = 'Procesar de inmediato';
$_['text_wait_x_days'] = 'Espera %d días';
$_['text_wait'] = 'Por favor, espere.';
$_['text_authorisation_reversed'] = 'La autorización se revirtió con éxito';
$_['text_refund_issued'] = 'Reembolso emitido con éxito';
$_['text_success'] = 'Éxito: ¡Ha modificado el módulo de servicio de Web de comercio seguro!';
$_['text_pending_settlement'] = 'Liquidación Pendiente';
$_['text_manual_settlement'] = 'Manual Settlement';
$_['text_suspended'] = 'Suspendido';
$_['text_cancelled'] = 'Cancelado';
$_['text_settling'] = 'Settling';
$_['text_settled'] = 'Arreglado';
$_['text_no_transactions'] = 'Ninguna transacción para mostrar';
$_['text_ok'] = 'Aceptar';
$_['text_denied'] = 'Denegado';
$_['text_transactions'] = 'transacciones';
$_['text_pending_settlement'] = 'Liquidación Pendiente';
$_['text_pending_settlement_manually_overriden'] = 'Liquidación pendiente, manualmente anulada';
$_['text_pending_suspended'] = 'Suspendido';
$_['text_pending_settled'] = 'Arreglado';

$_['entry_site_reference'] = 'Referencia del sitio';
$_['entry_username'] = 'Usuario';
$_['entry_password'] = 'Contraseña';
$_['entry_csv_username'] = 'CSV username';
$_['entry_csv_password'] = 'Contraseña CSV';
$_['entry_3d_secure'] = 'Use 3D Secure';
$_['entry_cards_accepted'] = 'Tarjetas aceptadas';
$_['entry_order_status'] = 'Estado del pedido';
$_['entry_failed_order_status'] = 'Estado del pedido fallido';
$_['entry_declined_order_status'] = 'Estado del pedido rechazado';
$_['entry_refunded_order_status'] = 'Estado de pedido reembolsado';
$_['entry_authorisation_reversed_order_status'] = 'Autorización de orden de estatus revertida';
$_['entry_settle_status'] = 'Estatus de Liquidacion';
$_['entry_settle_due_date'] = 'Establecer la fecha de vencimiento';
$_['entry_geo_zone'] = 'Zona geográfica';
$_['entry_sort_order'] = 'Ordenar por';
$_['entry_status'] = 'Estado';
$_['entry_total'] = 'Total ';
$_['entry_reverse_authorisation'] = 'Autorización revertida:';
$_['entry_refunded'] = 'Reembolsado:';
$_['entry_refund'] = 'Emitir el reembolso (%s):';
$_['entry_currency'] = 'Moneda';
$_['entry_status_code'] = 'Código de error';
$_['entry_payment_type'] = 'Tipo de pago';
$_['entry_request'] = 'Request';
$_['entry_settle_status'] = 'Settle Status';
$_['entry_date_from'] = 'Fecha desde';
$_['entry_date_to'] = 'Fecha hasta';
$_['entry_hour'] = 'Hora';
$_['entry_minute'] = 'Minutos';

$_['column_order_id'] = 'ID del Pedido';
$_['column_transaction_reference'] = 'Transaction reference';
$_['column_customer'] = 'Cliente';
$_['column_total'] = 'Total ';
$_['column_currency'] = 'Moneda';
$_['column_settle_status'] = 'Settle status';
$_['column_status'] = 'Estado';
$_['column_type'] = 'Tipo';
$_['column_payment_type'] = 'Tipo de pago';

$_['error_permission'] = 'No tiene permisos para modificar este módulo';
$_['error_site_reference'] = 'Sitio de Referencia requerido';
$_['error_cards_accepted'] = 'Tarjeta aceptada requerida';
$_['error_username'] = 'Se requiere un nombre de usuario';
$_['error_password'] = 'Se requiere una contraseña';
$_['error_connection'] = 'No se pudo conectar con comercio seguro';
$_['error_data_missing'] = 'Faltan datos';

$_['help_refund'] = 'Por favor incluye el punto decimal y la parte decimal del monto';
$_['help_csv_username'] = 'Username of the Transaction Download service';
$_['help_csv_password'] = 'Password of the Transaction Download service';
$_['help_total'] = 'El total a pagar que el pedido debe alcanzar para que este método se active';

$_['button_reverse_authorisation'] = 'Autorización revertida';
$_['button_refund'] = 'Reembolso';
$_['button_show'] = 'Mostrar';
$_['button_download'] = 'Descargar';

// Order page - payment tab
$_['text_payment_info'] = 'Información de pago';
$_['text_release_status'] = 'Pago Liberado';
$_['text_void_status'] = 'Autorización revertida';
$_['text_rebate_status'] = 'Pago reembolsado';
$_['text_order_ref'] = 'Ref. de pedido';
$_['text_order_total'] = 'Total autorizado';
$_['text_total_released'] = 'Total liberado';
$_['text_transactions'] = 'Transacciones';
$_['text_column_amount'] = 'Importe';
$_['text_column_type'] = 'Tipo';
$_['text_column_created'] = 'Creado';
$_['text_release_ok'] = 'Enviado con éxito';
$_['text_release_ok_order'] = 'Lanzamiento exitoso, orden de estatus actualizado - liquidado';
$_['text_rebate_ok'] = 'Descontado con éxito';
$_['text_rebate_ok_order'] = 'El reembolso fue exitoso, orden de estatus actualizado a reembolso';
$_['text_void_ok'] = 'La anulación fue exitosa, orden de estatus actualizado a anulado';

$_['text_confirm_void'] = '¿Estas seguro de que quieres revertir la autorización?';
$_['text_confirm_release'] = '¿Está seguro que desea reembolso el pago?';
$_['text_confirm_rebate'] = '¿Está seguro que desea reembolso el pago?';

$_['btn_release'] = 'Liberar';
$_['btn_rebate'] = 'Reembolso / devolución';
$_['btn_void'] = 'Autorización revertida';
