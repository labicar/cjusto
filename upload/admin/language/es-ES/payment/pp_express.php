<?php
// Heading
$_['heading_title']					 = 'PayPal Express Checkout';

// Text
$_['text_payment']				  	 = 'Pago';
$_['text_success']				 	 = 'Éxito: Ha modificado los datos de la cuenta PayPal Express Checkout!';
$_['text_edit']                      = 'Modificar PayPal Express Checkout';
$_['text_pp_express']				 = '<a target="_BLANK" href="https://www.paypal.com/uk/mrb/pal=V4T754QB63XXL"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro" title="PayPal Website Payment Pro iFrame" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']			 = 'Autorización';
$_['text_sale']						 = 'Rebajas';
$_['text_clear']					 = 'Borrar';
$_['text_browse']					 = 'Examinar';
$_['text_image_manager']			 = 'Administrador de imágenes';
$_['text_ipn']						 = 'IPN URL';
$_['text_paypal_join']               = 'Sign up for PayPal - save your settings first as this page will be refreshed';
$_['text_paypal_join_sandbox']       = 'Sign up for PayPal Sandbox - save your settings first as this page will be refreshed';

// Entry
$_['entry_username']				 = 'Nombre de usuario de la API';
$_['entry_password']				 = 'Contraseña API';
$_['entry_signature']				 = 'Firma de la API';
$_['entry_sandbox_username']		 = 'API Sandbox Username';
$_['entry_sandbox_password']		 = 'API Sandbox Password';
$_['entry_sandbox_signature']		 = 'API Sandbox Signature';
$_['entry_test']					 = 'Modo de prueba (Sandbox)';
$_['entry_method']					 = 'Método de transacción';
$_['entry_geo_zone']				 = 'Zona geográfica';
$_['entry_status']					 = 'Estado';
$_['entry_sort_order']				 = 'Ordenar por';
$_['entry_icon_sort_order']			 = 'Orden de los iconos';
$_['entry_debug']					 = 'Depurar registro';
$_['entry_total']					 = 'Total ';
$_['entry_currency']				 = 'Moneda por defecto';
$_['entry_recurring_cancellation']	 = 'Permitir a los clientes cancelar pagos recurrentes';
$_['entry_canceled_reversal_status'] = 'Estado de revocación cancelado';
$_['entry_completed_status']		 = 'Estado completado';
$_['entry_denied_status']			 = 'Estado denegado';
$_['entry_expired_status']			 = 'Estado caducado';
$_['entry_failed_status']			 = 'Estado fallido';
$_['entry_pending_status']			 = 'Estado pendiente';
$_['entry_processed_status']		 = 'Estado procesado';
$_['entry_refunded_status']			 = 'Estado reembolsado';
$_['entry_reversed_status']			 = 'Estado invertida';
$_['entry_voided_status']			 = 'Estado anulado';
$_['entry_display_checkout']		 = 'Mostrar icono quick checkout';
$_['entry_allow_notes']				 = 'Permitir notas';
$_['entry_logo']					 = 'Logo';
$_['entry_border_colour']			 = 'Color de borde de encabezado';
$_['entry_header_colour']			 = 'Color de fondo de cabecera';
$_['entry_page_colour']				 = 'Color de fondo de la página';

// Tab
$_['tab_general']					 = 'General';
$_['tab_api']				         = 'Detalles de API';
$_['tab_order_status']				 = 'Estado de orden';
$_['tab_checkout']					 = 'Realizar pedido';

// Help
$_['help_ipn']						 = 'Requerido para las suscripciones';
$_['help_total']					 = 'El total a pagar que el pedido debe alcanzar para que este método se active';
$_['help_logo']						 = 'Max 750px(w) x 90px(h) < br /> sólo debe utilizar un logotipo si tiene configurado  SSL.';
$_['help_colour']					 = 'Código de color de 6 caracteres HTML';
$_['help_currency']					 = 'Utilizado para las búsquedas de transacción';

// Error
$_['error_permission']				 = 'ADVERTENCIA: No tienes permiso para modificar el pago PayPal Express Checkout!';
$_['error_username']				 = 'Nombre de usuario API requerido!';
$_['error_password']				 = '¡Requiere contraseña API!';
$_['error_signature']				 = 'API firma requerida.';
$_['error_sandbox_username']	 	 = 'API Sandbox Username Required!';
$_['error_sandbox_password']		 = 'API Sandbox Password Required!';
$_['error_sandbox_signature']		 = 'API Sandbox Signature Required!';
$_['error_data']					 = 'Falta de petición de datos';
$_['error_timeout']					 = 'Agotado el tiempo de solicitud';
$_['error_api']						 = 'Paypal Authorization Error';
$_['error_api_sandbox']				 = 'Paypal Sandbox Authorization Error';