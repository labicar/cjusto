<?php
// Heading
$_['heading_title']					= 'Payza';

// Text
$_['text_payment']					= 'Pago';
$_['text_success']					= 'Éxito: ¡Ha modificado los detalles de la cuenta Payza!';
$_['text_edit']                     = 'Editar Payza';

// Entry
$_['entry_merchant']				= 'ID comerciante';
$_['entry_security']				= 'Código de seguridad';
$_['entry_callback']				= 'Alerta de URL';
$_['entry_total']					= 'Total ';
$_['entry_order_status']			= 'Estado del pedido';
$_['entry_geo_zone']				= 'Zona geográfica';
$_['entry_status']					= 'Estado';
$_['entry_sort_order']				= 'Ordenar por';

// Help
$_['help_callback']					= 'Esto se ha establecido en el panel de control de Payza. También necesita chequear el \'\'estado del IPN\'\' esté habilitado.';
$_['help_total']					= 'El total a pagar que el pedido debe alcanzar para que este método se active.';

// Error
$_['error_permission']				= 'Advertencia: ¡No tienes permiso para modificar el pago Payza!';
$_['error_merchant']				= '¡ID de comerciante requerida!';
$_['error_security']				= '¡Código de seguridad requerido!';