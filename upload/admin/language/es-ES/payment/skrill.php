<?php
// Heading
$_['heading_title']			  = 'Skrill';

// Text
$_['text_payment']			  = 'Pago';
$_['text_success']			  = 'Éxito: Ha modificado los detalles de Skrill.';
$_['text_edit']               = 'Edit Skrill';
$_['text_skrill']	     	  = '<a href="https://www.moneybookers.com/partners/?p=OpenCart" target="_blank"><img src="view/image/payment/skrill.png" alt="Skrill" title="Skrill" style="border: 1px solid #EEEEEE;" /></a>';

// Entry
$_['entry_email']			  = 'E-Mail';
$_['entry_secret']		      = 'Secreto';
$_['entry_total']			  = 'Total ';
$_['entry_order_status']	  = 'Estado del pedido';
$_['entry_pending_status']	  = 'Estado pendiente ';
$_['entry_canceled_status']	  = 'Estado cancelado';
$_['entry_failed_status']	  = 'Estado fallido';
$_['entry_chargeback_status'] = 'Chargeback Status';
$_['entry_geo_zone']		  = 'Zona geográfica';
$_['entry_status']			  = 'Estado';
$_['entry_sort_order']		  = 'Ordenar por';

// Help
$_['help_total']			  = 'El total a pagar que el pedido debe alcanzar para que este método se active.';

// Error
$_['error_permission']		  = '¡Advertencia: No tienes el permiso para modificar Skrill!';
$_['error_email']			  = '¡Correo electrónico requerido!';