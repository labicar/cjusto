<?php
// Heading
$_['heading_title']					= 'El reembolso fue exitoso';

// Text
$_['text_payment']					= 'Pago';
$_['text_success']					= 'Éxito: ¡Ha modificado los detalles de la cuenta GlobalPlay!';
$_['text_edit']                     = 'Edit Globalpay Remote';
$_['text_card_type']				= 'Tipo de tarjeta';
$_['text_enabled']					= 'Activado';
$_['text_use_default']				= 'Valor por defecto';
$_['text_merchant_id']				= 'ID comerciante';
$_['text_subaccount']				= 'Subcuenta';
$_['text_secret']					= 'Secreto compartido';
$_['text_card_visa']				= 'Visa';
$_['text_card_master']				= 'Mastercard';
$_['text_card_amex']				= 'American Express';
$_['text_card_switch']				= 'Switch/Maestro';
$_['text_card_laser']				= 'Láser';
$_['text_card_diners']				= 'Diners';
$_['text_capture_ok']				= 'La captura fue exitosa';
$_['text_capture_ok_order']			= 'La captura fue exitosa, estado de pedido se actualizo y se estableció con éxito';
$_['text_rebate_ok']				= 'El reembolso fue exitoso';
$_['text_rebate_ok_order']			= 'El reembolso fue exitoso, estado de pedido actualizado a reembolso';
$_['text_void_ok']					= 'La anulación fue exitosa, el estado de pedido actualizado a anulado';
$_['text_settle_auto']				= 'Auto';
$_['text_settle_delayed']			= 'Retrasado';
$_['text_settle_multi']				= 'Multi';
$_['text_ip_message']				= 'Debe proporcionar la dirección IP al gerente de la cuenta de GlobalPlay Antes de salir en vivo';
$_['text_payment_info']				= 'Información de pago';
$_['text_capture_status']			= 'Pago capturado';
$_['text_void_status']				= 'Pago anulado';
$_['text_rebate_status']			= 'Pago reembolsado';
$_['text_order_ref']				= 'Ref. de pedido';
$_['text_order_total']				= 'Total autorizado';
$_['text_total_captured']			= 'Total capturado';
$_['text_transactions']				= 'Transacciones';
$_['text_confirm_void']				= '¿Está seguro de que quiere invalidar el pago?';
$_['text_confirm_capture']			= '¿Está seguro que desea capturar el pago?';
$_['text_confirm_rebate']			= '¿Está seguro que desea reembolso el pago?';
$_['text_globalpay_remote']			= '<a target="_blank" href="https://resourcecentre.globaliris.com/getting-started.php?id=OpenCart"><img src="view/image/payment/globalpay.png" alt="Globalpay" title="Globalpay" style="border: 1px solid #EEEEEE;" /></a>';

// Column
$_['text_column_amount']			= 'Importe';
$_['text_column_type']				= 'Tipo';
$_['text_column_date_added']		= 'Creado';

// Entry
$_['entry_merchant_id']				= 'ID comerciante';
$_['entry_secret']					= 'Secreto compartido';
$_['entry_rebate_password']			= 'Contraseña de reembolso';
$_['entry_total']					= 'Total ';
$_['entry_sort_order']				= 'Orden de clasificación';
$_['entry_geo_zone']				= 'Zona geográfica';
$_['entry_status']					= 'Estado';
$_['entry_debug']					= 'Depurar registro';
$_['entry_auto_settle']				= 'Tipo de liquidación';
$_['entry_tss_check']				= 'Chequeos de TSS';
$_['entry_card_data_status']		= 'Ingreso de información de la tarjeta';
$_['entry_3d']						= 'Habilitar seguro 3D';
$_['entry_liability_shift']			= 'Acepte escenarios de cambio sin responsabilidad';
$_['entry_status_success_settled']	= 'Éxito - arreglado';
$_['entry_status_success_unsettled'] = 'Éxito - No arreglado';
$_['entry_status_decline']			= 'Rechazar';
$_['entry_status_decline_pending']	= 'Decline - offline auth';
$_['entry_status_decline_stolen']	= 'Decline - lost or stolen card';
$_['entry_status_decline_bank']		= 'Decline - bank error';
$_['entry_status_void']				= 'Anuladas';
$_['entry_status_rebate']			= 'Reembolsado';

// Help
$_['help_total']					= 'El total a pagar que el pedido debe alcanzar para que este método se active';
$_['help_card_select']				= 'Pídale al usuario que elija su tipo de tarjeta antes de que sean redirigidos';
$_['help_notification']				= 'Debe proporcionar esta URL a GlobalPlay para recibir las notificaciones de pago';
$_['help_debug']					= 'Debug permite escribir datos en un archivo de registro. Siempre deberá estar desactivado si no se indica lo contrario.';
$_['help_liability']				= 'Aceptar la responsabilidad significa que aún aceptará pagos cuando un usuario falle la seguridad 3D.';
$_['help_card_data_status']			= 'Ingrese los últimos 4 dígitos de las tarjetas, fecha de expiración, nombre y tipo de información bancaria';

// Tab
$_['tab_api']					    = 'Detalles de API';
$_['tab_account']				    = 'Cuentas';
$_['tab_order_status']				= 'Estado del pedido';
$_['tab_payment']					= 'Opciones de pago';

// Button
$_['button_capture']				= 'Captura';
$_['button_rebate']					= 'Reembolso / devolución';
$_['button_void']					= 'Vacío';

// Error
$_['error_merchant_id']				= 'El ID de comerciante es requerido';
$_['error_secret']					= 'Se requiere el secreto compartido';