<?php
// Heading
$_['heading_title']					= 'Web Payment Software';

// Text
$_['text_payment']					= 'Pagar';
$_['text_success']					= 'Success: You have modified Web Payment Software account details!';
$_['text_edit']                     = 'Editar AWeb Payment Software';
$_['text_web_payment_software']		= '<a href="http://www.web-payment-software.com/" target="_blank"><img src="view/image/payment/wps-logo.jpg" alt="Web Payment Software" title="Web Payment Software" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_test']						= 'Test';
$_['text_live']						= 'En vivo';
$_['text_authorization']			= 'Autorización';
$_['text_capture']					= 'Captura';

// Entry
$_['entry_login']					= 'ID comerciante';
$_['entry_key']						= 'Clave de comerciante';
$_['entry_mode']					= 'Modo de operación';
$_['entry_method']					= 'Método de transacción';
$_['entry_total']					= 'Total ';
$_['entry_order_status']			= 'Estado del pedido';
$_['entry_geo_zone']				= 'Zona geográfica';
$_['entry_status']					= 'Estado';
$_['entry_sort_order']				= 'Ordenar por';

// Help
$_['help_total']					= 'El total a pagar que el pedido debe alcanzar para que este método se active.';

// Error
$_['error_permission']				= 'Warning: You do not have permission to modify payment Web Payment Software!';
$_['error_login']					= 'Login ID Required!';
$_['error_key']						= 'Transaction Key Required!';