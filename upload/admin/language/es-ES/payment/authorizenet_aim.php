<?php
// Heading
$_['heading_title']					= 'Authorize.Net (AIM)';

// Text
$_['text_payment']					= 'Pagar';
$_['text_success']					= 'Exito: Usted ha modificado los datos de la cuenta de Authorize.Net (AIM)!';
$_['text_edit']                     = 'Editar Authorize.Net (AIM)';
$_['text_test']						= 'Test';
$_['text_live']						= 'En vivo';
$_['text_authorization']			= 'Autorización';
$_['text_capture']					= 'Captura';
$_['text_authorizenet_aim']			= '<a onclick="window.open(\'http://reseller.authorize.net/application/?id=5561103\');"><img src="view/image/payment/authorizenet.png" alt="Authorize.Net" title="Authorize.Net" style="border: 1px solid #EEEEEE;" /></a>';

// Entry
$_['entry_login']					= 'ID de inicio de sesión';
$_['entry_key']						= 'Clave de transacción';
$_['entry_hash']					= 'Hash MD5';
$_['entry_server']					= 'Servidor de la transacción';
$_['entry_mode']					= 'Modo de operación';
$_['entry_method']					= 'Método de transacción';
$_['entry_total']					= 'Total ';
$_['entry_order_status']			= 'Estado del pedido';
$_['entry_geo_zone']				= 'Zona geográfica';
$_['entry_status']					= 'Estado';
$_['entry_sort_order']				= 'Ordenar por';

// Help
$_['help_total']					= 'El total a pagar que el pedido debe alcanzar para que este método se active.';

// Error
$_['error_permission']				= 'Advertencia: Usted no tiene autorización para modificar el pago de Authorize.Net (SIM)!';
$_['error_login']					= '¡ID de inicio de sesión requerido!';
$_['error_key']						= 'Clave de transacción requerida!';