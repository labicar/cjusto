<?php
// Heading
$_['heading_title']		 = 'Envío gratuito';

// Text
$_['text_payment']		 = 'Pago';
$_['text_success']		 = 'Éxito: Ha modificado módulo de pago libre!';
$_['text_edit']          = 'Editar Pago libre';

// Entry
$_['entry_order_status'] = 'Estado del pedido';
$_['entry_status']		 = 'Estado';
$_['entry_sort_order']	 = 'Ordenar por';

// Error
$_['error_permission']	  = 'ADVERTENCIA: No tienes permiso para modificar pago libre!';