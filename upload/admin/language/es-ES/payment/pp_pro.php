<?php
// Heading
$_['heading_title']					= 'PayPal Pro';

// Text
$_['text_success']					= 'Success: You have modified PayPal Website Payment Pro Checkout account details!';
$_['text_edit']                     = 'Editar PayPal Pro';
$_['text_pp_pro']					= '<a target="_BLANK" href="https://www.paypal.com/uk/mrb/pal=V4T754QB63XXL"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro" title="PayPal Website Payment Pro iFrame" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']			= 'Autorización';
$_['text_sale']						= 'Rebajas';

// Entry
$_['entry_username']				= 'Nombre de usuario de la API';
$_['entry_password']				= 'Contraseña API';
$_['entry_signature']				= 'Firma API';
$_['entry_test']					= 'Modo de prueba';
$_['entry_transaction']				= 'Método de transacción:';
$_['entry_total']					= 'Total ';
$_['entry_order_status']			= 'Estado del pedido';
$_['entry_geo_zone']				= 'Zona geográfica';
$_['entry_status']					= 'Estado';
$_['entry_sort_order']				= 'Ordenar por';

// Help
$_['help_test']						= '¿Utilizar el real o el servidor de prueba (sandbox) para procesar transacciones?';
$_['help_total']					= 'El total a pagar que el pedido debe alcanzar para que este método se active';

// Error
$_['error_permission']				= 'ADVERTENCIA: ¡No tienes permiso para modificar el pago PayPal Website Payment Pro Checkout!';
$_['error_username']				= 'Nombre de usuario API requerido!';
$_['error_password']				= '¡Requiere contraseña API!';
$_['error_signature']				= 'API firma requerida.';