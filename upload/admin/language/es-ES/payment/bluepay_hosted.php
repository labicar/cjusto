<?php
// Heading
$_['heading_title']            = 'BluePay Hosted Form';

// Text
$_['text_payment']             = 'Pagar';
$_['text_success']             = 'Success: You have modified BluePay Hosted Form account details!';
$_['text_edit']                = 'Edit BluePay Hosted Form';
$_['text_bluepay_hosted'] = '<a href="http://www.bluepay.com/preferred-partner/opencart" target="_blank"><img src="view/image/payment/bluepay.jpg" alt="BluePay Hosted Form" title="BluePay Hosted Form" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_test']                = 'Test';
$_['text_live']                = 'En vivo';
$_['text_sale']                = 'Rebajas';
$_['text_authenticate']        = 'Autentificar';
$_['text_release_ok']          = 'Enviado con éxito';
$_['text_release_ok_order']    = 'Enviado con éxito';
$_['text_rebate_ok']           = 'Descontado con éxito';
$_['text_rebate_ok_order']     = 'El reembolso fue exitoso, orden de estatus actualizado a reembolso';
$_['text_void_ok']             = 'La anulación fue exitosa, orden de estatus actualizado a anulado';
$_['text_payment_info']        = 'Información de pago';
$_['text_release_status']			= 'Pago Liberado';
$_['text_void_status']				= 'Pago anulado';
$_['text_rebate_status']			= 'Pago reembolsado';
$_['text_order_ref']				= 'Ref. de pedido';
$_['text_order_total']				= 'Total autorizado';
$_['text_total_released']			= 'Total liberado';
$_['text_transactions']				= 'Transacciones';
$_['text_column_amount']			= 'Importe';
$_['text_column_type']				= 'Tipo';
$_['text_column_date_added']		= 'Agregar Fecha';
$_['text_confirm_void']				= '¿Está seguro de que quiere invalidar el pago?';
$_['text_confirm_release']			= '¿Está seguro que desea reembolso el pago?';
$_['text_confirm_rebate']			= '¿Está seguro que desea reembolso el pago?';

// Entry
$_['entry_account_name']			= 'Nombre de cuenta';
$_['entry_account_id']				= 'Identificación de la cuenta';
$_['entry_secret_key']				= 'Clave secreta';
$_['entry_test']					= 'Modo de prueba';
$_['entry_transaction']				= 'Método de transacción';
$_['entry_card_amex']				= 'Amex';
$_['entry_card_discover']			= 'Descubrir';
$_['entry_total']					= 'Total ';
$_['entry_order_status']			= 'Estado del pedido';
$_['entry_geo_zone']				= 'Zona geográfica';
$_['entry_status']					= 'Estado';
$_['entry_sort_order']				= 'Ordenar por';
$_['entry_debug']					= 'Depurar registro';

// Help
$_['help_total']					= 'El total a pagar que el pedido debe alcanzar para que este método se active.';
$_['help_debug']					= 'Enabling debug will write sensitive data to a log file. You should always disable unless instructed otherwise';
$_['help_transaction']				= 'Transaction method MUST be set to Payment to allow subscription payments';
$_['help_cron_job_token']			= 'Hacer esto largo y difícil de adivinar';
$_['help_cron_job_url']				= 'Configurar una tarea programada para llamar a esta URL';

// Button
$_['btn_release']					= 'Lanzamiento';
$_['btn_rebate']					= 'Reembolso / devolución';
$_['btn_void']						= 'Vacío';

// Error
$_['error_permission']				= 'Warning: You do not have permission to modify payment BluePay!';
$_['error_account_name']			= '¡Nombre de cuenta requerido!';
$_['error_account_id']				= 'Account ID Required!';
$_['error_secret_key']				= '¡Clave secreta necesaria!';