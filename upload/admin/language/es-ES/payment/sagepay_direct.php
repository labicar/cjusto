<?php
// Heading
$_['heading_title']					= 'SagePay Direct';

// Text
$_['text_payment']					= 'Pagar';
$_['text_success']					= '! Éxito: Ha modificado los detalles de la cuenta de SagePay!';
$_['text_edit']                     = 'Editar SagePay Direct';
$_['text_sagepay_direct']			= '<a href="https://support.sagepay.com/apply/default.aspx?PartnerID=E511AF91-E4A0-42DE-80B0-09C981A3FB61" target="_blank"><img src="view/image/payment/sagepay.png" alt="SagePay" title="SagePay" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_sim']						= 'Simulador';
$_['text_test']						= 'Test';
$_['text_live']						= 'En vivo';
$_['text_defered']					= 'Aplazado';
$_['text_authenticate']				= 'Autentificar';
$_['text_release_ok']				= 'Enviado con éxito';
$_['text_release_ok_order']			= 'El lanzamiento fue un éxito, el estado del pedido ha sido actualizado con éxito';
$_['text_rebate_ok']				= 'Descontado con éxito';
$_['text_rebate_ok_order']			= 'El reembolso fue exitoso, orden de estatus actualizado a reembolso';
$_['text_void_ok']					= 'La anulación fue exitosa, orden de estatus actualizado a anulado';
$_['text_payment_info']				= 'Información de pago';
$_['text_release_status']			= 'Pago Liberado';
$_['text_void_status']				= 'Pago anulado';
$_['text_rebate_status']			= 'Pago reembolsado';
$_['text_order_ref']				= 'Ref. de pedido';
$_['text_order_total']				= 'Total autorizado';
$_['text_total_released']			= 'Total liberado';
$_['text_transactions']				= 'Transacciones';
$_['text_column_amount']			= 'Importe';
$_['text_column_type']				= 'Tipo';
$_['text_column_date_added']		= 'Creado';
$_['text_confirm_void']				= '¿Está seguro de que quiere invalidar el pago?';
$_['text_confirm_release']			= '¿Está seguro que desea reembolso el pago?';
$_['text_confirm_rebate']			= '¿Está seguro que desea reembolso el pago?';

// Entry
$_['entry_vendor']					= 'Proveedor';
$_['entry_test']					= 'Modo de prueba';
$_['entry_transaction']				= 'Método de transacción';
$_['entry_total']					= 'Total ';
$_['entry_order_status']			= 'Estado del pedido';
$_['entry_geo_zone']				= 'Zona geográfica';
$_['entry_status']					= 'Estado';
$_['entry_sort_order']				= 'Ordenar por';
$_['entry_debug']					= 'Depurar registro';
$_['entry_card']					= 'Tarjetas de tiendas';
$_['entry_cron_job_token']			= 'Token secreto';
$_['entry_cron_job_url']			= 'URL de cronjob';
$_['entry_last_cron_job_run']		= 'Last cron job\'s run time:';

// Help
$_['help_total']					= 'El total a pagar que el pedido debe alcanzar para que este método se active.';
$_['help_debug']					= 'Enabling debug will write sensitive data to a log file. You should always disable unless instructed otherwise';
$_['help_transaction']				= 'Transaction method MUST be set to Payment to allow subscription payments';
$_['help_cron_job_token']			= 'Hacer esto largo y difícil de adivinar';
$_['help_cron_job_url']				= 'Configurar una tarea programada para llamar a esta URL';

// Button
$_['btn_release']					= 'Lanzamiento';
$_['btn_rebate']					= 'Reembolso / devolución';
$_['btn_void']						= 'Vacío';

// Error
$_['error_permission']				= 'Warning: You do not have permission to modify payment SagePay!';
$_['error_vendor']					= 'Vendor ID Required!';