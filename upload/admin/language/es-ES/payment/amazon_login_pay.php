<?php
//Headings
$_['heading_title']                 = 'Iniciar sesión y pagar con Amazon';

//Text
$_['text_success']                  = 'Iniciar sesión y pagar con el modulo de Amazon ha sido actualizado';
$_['text_ipn_url']					= 'URL de cronjob';
$_['text_ipn_token']				= 'Token secreto';
$_['text_us']						= 'EEUU';
$_['text_germany']                  = 'Alemania';
$_['text_uk']                       = 'Reino Unido';
$_['text_live']                     = 'En vivo';
$_['text_sandbox']                  = 'Sandbox';
$_['text_auth']						= 'Autorización';
$_['text_payment']                  = 'Pago';
$_['text_no_capture']               = '--- Sin Captura Automática ---';
$_['text_all_geo_zones']            = 'Todas las Geo zonas';
$_['text_button_settings']          = 'Configuración de botón de pago';
$_['text_colour']                   = 'Color';
$_['text_orange']                   = 'Naranja';
$_['text_tan']                      = 'Tan';
$_['text_white']                    = 'Blanco';
$_['text_light']                    = 'Ligero';
$_['text_dark']                     = 'Oscuro';
$_['text_size']                     = 'Tamaño';
$_['text_medium']                   = 'Mediano';
$_['text_large']                    = 'Grande';
$_['text_x_large']                  = 'Extra grande';
$_['text_background']               = 'Fondo';
$_['text_edit']						= 'Editar inicio de sesión y pagar con Amazon';
$_['text_amazon_login_pay']         = '<a href="http://go.amazonservices.com/opencart.html" target="_blank" title="Sign-up to Login and Pay with Amazon"><img src="view/image/payment/amazon.png" alt="Login and Pay with Amazon" title="Login and Pay with Amazon" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_amazon_join']              = '<a href="http://go.amazonservices.com/opencart.html" target="_blank" title="Sign-up to Login and Pay with Amazon"><u>Regístrese para iniciar sesión y pagar con Amazon</u></a>';
$_['entry_login_pay_test']          = 'Modo de prueba';
$_['entry_login_pay_mode']          = 'Modo de pago';
$_['text_payment_info']				= 'Información de pago';
$_['text_capture_ok']				= 'La captura fue exitosa';
$_['text_capture_ok_order']			= 'La captura fue exitosa, la autorización ha sido capturada por completo';
$_['text_refund_ok']				= 'El reembolso fue solicitado con éxito';
$_['text_refund_ok_order']			= 'El reembolso fue solicitado con éxito, cantidad totalmente reembolsada';
$_['text_cancel_ok']				= 'La cancelación fue exitosa, estado de la orden actualizado a cancelado';
$_['text_capture_status']			= 'Pago capturado';
$_['text_cancel_status']			= 'Pago cancelado';
$_['text_refund_status']			= 'Pago reembolsado';
$_['text_order_ref']				= 'Ref. de pedido';
$_['text_order_total']				= 'Total autorizado';
$_['text_total_captured']			= 'Total capturado';
$_['text_transactions']				= 'Transacciones';
$_['text_column_authorization_id']	= 'ID de la autorización de Amazon';
$_['text_column_capture_id']		= 'ID de captura de Amazon';
$_['text_column_refund_id']			= 'ID de reembolso de Amazon';
$_['text_column_amount']			= 'Importe';
$_['text_column_type']				= 'Tipo';
$_['text_column_status']			= 'Estado';
$_['text_column_date_added']		= 'Fecha creación';
$_['text_confirm_cancel']			= '¿Está seguro que desea cancelar el pago?';
$_['text_confirm_capture']			= '¿Está seguro que desea capturar el pago?';
$_['text_confirm_refund']			= '¿Está seguro que desea reembolso el pago?';
$_['text_minimum_total']            = 'Pedido Mínimo Total';
$_['text_geo_zone']                 = 'Zona geográfica';
$_['text_status']                   = 'Estado';
$_['text_declined_codes']           = 'Códigos de Declinación de Prueba';
$_['text_sort_order']               = 'Ordenar por';
$_['text_amazon_invalid']           = 'MetodoDePagoInvalido';
$_['text_amazon_rejected']          = 'AmazonRechazó';
$_['text_amazon_timeout']           = 'Tiempo terminado en transaccion';
$_['text_amazon_no_declined']       = 'Autorización automática denegada';

// Columns
$_['column_status']                 = 'Estado';

//entry
$_['entry_merchant_id']             = 'ID comerciante';
$_['entry_access_key']              = 'Tecla de acceso';
$_['entry_access_secret']           = 'Clave secreta';
$_['entry_client_id']               = 'Client ID';
$_['entry_client_secret']           = 'Cliente Secreto';
$_['entry_marketplace']             = 'Marketplace';
$_['entry_capture_status']          = 'Estado de captura automática';
$_['entry_pending_status']          = 'Estado de pedido pendiente';
$_['entry_ipn_url']					= 'URL del IPN';
$_['entry_ipn_token']				= 'Token secreto';
$_['entry_debug']					= 'Depurar registro';


// Help
$_['help_pay_mode']					= 'El pago solo está disponible para el mercado estadounidense';
$_['help_capture_status']			= 'Seleccione el estado del pedido que activará la captura automática de un pago autorizado';
$_['help_ipn_url']					= 'Establecer esto como su URL comercial en la Central del Vendedor de Amazon';
$_['help_ipn_token']				= 'Hacer esto largo y difícil de adivinar';
$_['help_debug']					= 'Al habilitar la depuración se escribirán los datos confidenciales en un archivo de registro. Siempre debe desactivar a menos que se indique lo contrario';
$_['help_declined_codes']			= 'Esto es solo para propósitos de prueba';

// Order Info
$_['tab_order_adjustment']          = 'Ajustes de pedido';

// Errors
$_['error_permission']              = 'No tiene permisos para modificar este módulo';
$_['error_merchant_id']			    = 'El ID de comerciante es requerido';
$_['error_access_key']			    = 'Se requiere una clave de acceso';
$_['error_access_secret']		    = 'Clave secreta es requerida';
$_['error_client_id']			    = 'Es requerida la ID del cliente';
$_['error_client_secret']			= 'Es requerida la clave del cliente';
$_['error_pay_mode']				= 'El pago solo está disponible para el mercado estadounidense';
$_['error_curreny']                 = 'Tu tienda debe tener%s de moneda instalado y habilitado';
$_['error_upload']                  = 'Fallo en la subida';
$_['error_data_missing'] 			= 'Faltan datos requeridos';

// Buttons
$_['button_capture']				= 'Captura';
$_['button_refund']					= 'Reembolso';
$_['button_cancel']					= 'Cancelar';
