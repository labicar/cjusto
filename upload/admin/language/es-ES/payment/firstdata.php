<?php
// Heading
$_['heading_title']					 = 'Primera conexión de datos EMEA (3D Seguro Habilitado)';

// Text
$_['text_payment']					 = 'Pago';
$_['text_success']					 = 'Éxito: ¡Ha modificado los detalles de su cuenta de Primeros Datos!';
$_['text_edit']                      = 'Editar primeros datos de conexión EMEA (3D Segura habilitada)';
$_['text_notification_url']			 = 'URL de notificación';
$_['text_live']						 = 'En vivo';
$_['text_demo']						 = 'Demo';
$_['text_enabled']					 = 'Activado';
$_['text_merchant_id']				 = 'Identificación de la tienda';
$_['text_secret']					 = 'Secreto compartido';
$_['text_capture_ok']				 = 'La captura fue exitosa';
$_['text_capture_ok_order']			 = 'La captura fue exitosa, estado de pedido se actualizo y se estableció con éxito';
$_['text_void_ok']					 = 'La anulación fue exitosa, el estado de pedido actualizad a anulado';
$_['text_settle_auto']				 = 'Rebajas';
$_['text_settle_delayed']			 = 'Pre autenticación';
$_['text_success_void']				 = 'La operación ha sido anulada';
$_['text_success_capture']			 = 'La transacción ha sido capturada';
$_['text_firstdata']				 = '<img src="view/image/payment/firstdata.png" alt="First Data" title="First Data" style="border: 1px solid #EEEEEE;" />';
$_['text_payment_info']				 = 'Información de pago';
$_['text_capture_status']			 = 'Pago capturado';
$_['text_void_status']				 = 'Pago anulado';
$_['text_order_ref']				 = 'Ref. de pedido';
$_['text_order_total']				 = 'Total autorizado';
$_['text_total_captured']			 = 'Total capturado';
$_['text_transactions']				 = 'Transacciones';
$_['text_column_amount']			 = 'Importe';
$_['text_column_type']				 = 'Tipo';
$_['text_column_date_added']		 = 'Creado';
$_['text_confirm_void']				 = '¿Está seguro de que quiere invalidar el pago?';
$_['text_confirm_capture']			 = '¿Está seguro que desea capturar el pago?';

// Entry
$_['entry_merchant_id']				 = 'ID Tienda';
$_['entry_secret']					 = 'Secreto compartido';
$_['entry_total']					 = 'Total ';
$_['entry_sort_order']				 = 'Orden de clasificación';
$_['entry_geo_zone']				 = 'Zona geográfica';
$_['entry_status']					 = 'Estado';
$_['entry_debug']					 = 'Depurar registro';
$_['entry_live_demo']				 = 'Live / Demo';
$_['entry_auto_settle']			  	 = 'Tipo de liquidación';
$_['entry_card_select']				 = 'Seleccione tarjeta';
$_['entry_tss_check']				 = 'Chequeos de TSS';
$_['entry_live_url']				 = 'URL de conexión en vivo';
$_['entry_demo_url']				 = 'URL de demostración en vivo';
$_['entry_status_success_settled']	 = 'Éxito - arreglado';
$_['entry_status_success_unsettled'] = 'Éxito - No arreglado';
$_['entry_status_decline']		 	 = 'Rechazar';
$_['entry_status_void']				 = 'Anuladas';
$_['entry_enable_card_store']		 = 'Habilitar fichas de almacenamiento de tarjetas';

// Help
$_['help_total']					 = 'El total a pagar que el pedido debe alcanzar para que este método se active';
$_['help_notification']				 = 'Debe proporcionar esta URL a los Primeros Datos para recibir las notificaciones de pago';
$_['help_debug']					 = 'Al habilitar la depuración se escribirán los datos confidenciales en un archivo de registro. Siempre debe desactivar a menos que se indique lo contrario';
$_['help_settle']					 = 'Si usa la pre-autorización, debe completar una acción de post-autenticación en un plazo de 3 a 5 días, de lo contrario su transacción de cancelará'; 

// Tab
$_['tab_account']					 = 'Información de la API';
$_['tab_order_status']				 = 'Estado del pedido';
$_['tab_payment']					 = 'Opciones de pago';
$_['tab_advanced']					 = 'Avanzado';

// Button
$_['button_capture']				 = 'Captura';
$_['button_void']					 = 'Vacío';

// Error
$_['error_merchant_id']				 = 'Se requiere ID de la tienda';
$_['error_secret']					 = 'Se requiere el secreto compartido';
$_['error_live_url']				 = 'Se requiere el URL en vivo';
$_['error_demo_url']				 = 'Se requiere el demo de la URL';
$_['error_data_missing']			 = 'Faltan datos';
$_['error_void_error']				 = 'No se pudo anular la transacción';
$_['error_capture_error']			 = 'No se pudo capturar la transacción';