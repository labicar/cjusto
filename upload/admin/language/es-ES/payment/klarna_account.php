<?php
// Heading
$_['heading_title']					= 'Cuenta Klarna';

// Text
$_['text_payment']					= 'Pago';
$_['text_success']					= 'Éxito: ¡Modificaste el Módulo de pago Klarna!';
$_['text_edit']                     = 'Editar cuenta Klarna';
$_['text_klarna_account']			= '<a href="https://merchants.klarna.com/signup?locale=en&partner_id=d5c87110cebc383a826364769047042e777da5e8&utm_campaign=Platform&utm_medium=Partners&utm_source=Opencart" target="_blank"><img src="https://cdn.klarna.com/public/images/global/logos/v1/basic/global_basic_logo_std_blue-black.png?width=60&eid=opencart" alt="Klarna Account" title="Klarna Account" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_live']						= 'En vivo';
$_['text_beta']						= 'Beta';
$_['text_sweden']					= 'Suecia';
$_['text_norway']					= 'Noruega';
$_['text_finland']					= 'Finlandia';
$_['text_denmark']					= 'Dinamarca';
$_['text_germany']					= 'Alemania';
$_['text_netherlands']				= 'Países Bajos';

// Entry
$_['entry_merchant']				= 'ID Klarna Merchant';
$_['entry_secret']					= 'Klarna Secret';
$_['entry_server']					= 'Servidor';
$_['entry_total']					= 'Total ';
$_['entry_pending_status']			= 'Estado pendiente';
$_['entry_accepted_status']			= 'Estado aceptado';
$_['entry_geo_zone']				= 'Zona geográfica';
$_['entry_status']					= 'Estado';
$_['entry_sort_order']				= 'Ordenar por';

// Help
$_['help_merchant']					= '(Id de estore) para usar para el servicio de Klarna (proporcionado por Klarna).';
$_['help_secret']					= 'Secreto compartido para usar con el servicio de Klarna (proporcionado por Klarna).';
$_['help_total']					= 'El total a pagar que el pedido debe alcanzar para que este método se active.';

// Error
$_['error_permission']				= 'Advertencia: ¡No tienes permiso para modificar el pago de parte de Klarna!';
$_['error_pclass']					= 'No se puede recuperar la pClass para %s. Código de error: %s; Mensaje de error: %s';
$_['error_curl']					= 'Curl Error - Code: %d; Message: %s';
$_['error_log']						= 'Hubo errores actualizar el módulo. Por favor, compruebe el archivo de registro.';