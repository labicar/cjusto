<?php
// Heading
$_['heading_title']					= 'Paymate';

// Text
$_['text_payment']					= 'Pago';
$_['text_success']					= 'Success: You have modified Paymate account details!';
$_['text_edit']                     = 'Editar Paymate';
$_['text_paymate']					= '<img src="view/image/payment/paymate.png" alt="Paymate" title="Paymate" style="border: 1px solid #EEEEEE;" />';

// Entry
$_['entry_username']				= 'Paymate Username';
$_['entry_password']				= 'Contraseña';
$_['entry_test']					= 'Modo de prueba';
$_['entry_total']					= 'Total ';
$_['entry_order_status']			= 'Estado del pedido';
$_['entry_geo_zone']				= 'Zona geográfica';
$_['entry_status']					= 'Estado';
$_['entry_sort_order']				= 'Ordenar por';

// Help
$_['help_password']					= 'Sólo tiene que utilizar alguna contraseña aleatoria. Esto se utilizará para asegurarse de que la información de pago no es interferida después de ser enviada a la pasarela de pago.';
$_['help_total']					= 'El total a pagar que el pedido debe alcanzar para que este método se active.';

// Error
$_['error_permission']				= 'ADVERTENCIA: ¡No tienes permiso para modificar el pago PayPal!';
$_['error_username']				= 'Paymate Username required!';
$_['error_password']				= '¡Contraseña requerida!';