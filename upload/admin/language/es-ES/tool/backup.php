<?php
// Heading
$_['heading_title']    = 'Copia de resguardo y restauración';

// Text
$_['text_backup']      = 'Descargar copia de seguridad';
$_['text_success']     = 'Éxito: Se ha importado con éxito su base de datos!';
$_['text_list']        = 'Subir lista';

// Entry
$_['entry_restore']    = 'Restaurar copia de resguardo';
$_['entry_backup']     = 'Copia de resguardo';

// Error
$_['error_permission'] = 'Advertencia: No tienes permiso para modificar las copias de resguardo!';
$_['error_backup']     = 'Advertencia: Se debe seleccionar al menos una tabla para copia de seguridad!';
$_['error_empty']      = 'Advertencia: El archivo subido estaba vacío!';