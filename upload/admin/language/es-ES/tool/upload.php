<?php
// Heading
$_['heading_title']     = 'Subidas';

// Text
$_['text_success']      = 'Éxito: Ha modificado las subidas!';
$_['text_list']         = 'Subir lista';

// Column
$_['column_name']       = 'Nombre de la subida';
$_['column_filename']   = 'Nombre del Archivo';
$_['column_date_added'] = 'Agregar Fecha';
$_['column_action']     = 'Acción';

// Entry
$_['entry_name']        = 'Nombre de la subida';
$_['entry_filename']    = 'Nombre del Archivo';
$_['entry_date_added'] 	= 'Agregar Fecha';

// Error
$_['error_permission']  = 'Advertencia: No tienes permiso para modificar las subidas!';