<?php
// Heading
$_['heading_title']         = 'Servicio Postal de Estados Unidos';

// Text
$_['text_shipping']         = 'Envío';
$_['text_success']          = 'Éxito: Ha modificado Servicio Postal de Estados Unidos.';
$_['text_edit']             = 'Editar el envío por Servicio Postal de Estados Unidos';
$_['text_domestic_00']      = 'Paquete por correo primera clase';
$_['text_domestic_01']      = 'Sobre grande por Correo de primera clase';
$_['text_domestic_02']      = 'Carta por correo de primera clase';
$_['text_domestic_03']      = 'Postal por correo de primera clase';
$_['text_domestic_1']       = 'Correo prioritario';
$_['text_domestic_2']       = 'Correo express en espera para recoger';
$_['text_domestic_3']       = 'Correo Express (EMS)';
$_['text_domestic_4']       = 'Paquete postal (encomienda)';
$_['text_domestic_5']       = 'Atado de materiales impresos (revistas/periódicos)';
$_['text_domestic_6']       = 'Correo de medios de comunicación';
$_['text_domestic_7']       = 'Librería';
$_['text_domestic_12']      = 'Estampado Postal primera clase';
$_['text_domestic_13']      = 'Tarifa estandard Express Mail (EMS) para sobre ';
$_['text_domestic_16']      = 'Tarifa estandard Priority Mail para sobre ';
$_['text_domestic_17']      = 'Tarifa estandar de Correo prioritario para cajas';
$_['text_domestic_18']      = 'Identificaciones y claves de correo prioritario';
$_['text_domestic_19']      = 'Identificaciones y llaves de correo de primera clase';
$_['text_domestic_22']      = 'Tarifa estandar de correo prioritario para cajas grandes';
$_['text_domestic_23']      = 'Correo Express Domigos/Días festivos';
$_['text_domestic_25']      = 'Tarifa estandar de correo express para sobres en Domingos/Días festivos';
$_['text_domestic_27']      = 'Tarifa estandar de correo express para sobres ocurre';
$_['text_domestic_28']      = 'Tarifa estandar de correo prioritario para cajas pequeñas';
$_['text_international_1']  = 'Correo Express Internacional';
$_['text_international_2']  = 'Correo prioritario internacional';
$_['text_international_4']  = 'Global Express garantizado (Documentos y no-documentos)';
$_['text_international_5']  = 'Global Express garantizado documento utilizado';
$_['text_international_6']  = 'Global Express garantizado documento no rectangular';
$_['text_international_7']  = 'Global Express Garantizado para no-documentos no-rectangulares';
$_['text_international_8']  = 'Tarifa estandar de correo prioritario para sobres';
$_['text_international_9']  = 'Tarifa estandar de correo prioritario para cajas';
$_['text_international_10'] = 'Tarifa estandar Correo express internacional para sobres';
$_['text_international_11'] = 'Tarifa estandar de correo prioritario para cajas grandes';
$_['text_international_12'] = 'Global Express Garantizado para sobres';
$_['text_international_13'] = 'Correo de primera clase Internacional para cartas';
$_['text_international_14'] = 'Correo estandar de primera clase internacional';
$_['text_international_15'] = 'Encomiendas internacionales Correo primera clase';
$_['text_international_16'] = 'Tarifa estandar de correo prioritario para cajas pequeñas';
$_['text_international_21'] = 'Postales';
$_['text_regular']          = 'Regular';
$_['text_large']            = 'Grande';
$_['text_rectangular']      = 'Rectangular';
$_['text_non_rectangular']  = 'No rectangular';
$_['text_variable']         = 'Variable';

// Entry
$_['entry_user_id']         = 'ID de usuario';
$_['entry_postcode']        = 'Código postal';
$_['entry_domestic']        = 'Servicios domésticos';
$_['entry_international']   = 'Servicios Internacionales';
$_['entry_size']            = 'Tamaño';
$_['entry_container']       = 'Contenedor';
$_['entry_machinable']      = 'Manufacturable';
$_['entry_dimension']       = 'Dimensiones (L x A x A)';
$_['entry_length']          = 'Longitud';
$_['entry_height']          = 'Alto';
$_['entry_width']           = 'Ancho';
$_['entry_display_time']    = 'Despliega tiempo de entrega';
$_['entry_display_weight']  = 'Despliega peso de entrega';
$_['entry_weight_class']    = 'Medida de Peso';
$_['entry_tax']             = 'Tipo de Impuesto';
$_['entry_geo_zone']        = 'Zona geográfica';
$_['entry_status']          = 'Estado';
$_['entry_sort_order']      = 'Ordenar por';
$_['entry_debug']      		= 'Modo de depuración';

// Help
$_['help_dimension']        = 'Dimensiones promedio del empaque para envío. Dimensiones del producto no son usadas para envío en este momento.';
$_['help_display_time']     = '¿Quieres mostrar el tiempo de envío? (p.ej. envío dentro de 3 a 5 días)';
$_['help_display_weight']   = '¿Quieres mostrar el peso del envío? (p.ej. peso de entrega: 2,7674 kgs)';
$_['help_weight_class']     = 'Debe establecerse en libras.';
$_['help_debug']      		= 'Guardar datos enviados/recibidos en el registro del sistema';

// Error
$_['error_permission']      = 'ADVERTENCIA: No tienes permiso para modificar el Servicio Postal de Estados Unidos!';
$_['error_user_id']         = '¡Nombre de usuario requerido!';
$_['error_postcode']        = '¡Código postal requerido!';
$_['error_width']        	= '¡Anchura requerida!';
$_['error_length']        	= 'Longitud requerida!';
$_['error_height']        	= '¡Altura requerida!';
$_['error_girth']        	= '¡Circunferencia requerida!';