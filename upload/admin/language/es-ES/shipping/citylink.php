<?php
// Heading
$_['heading_title']    = 'Citylink';

// Text
$_['text_shipping']    = 'Envío';
$_['text_success']     = 'Éxito: ¡Has modificado el envío Citylink!';
$_['text_edit']        = 'Editar envío de Citylink';

// Entry
$_['entry_rate']       = 'Tarifas de Citylink';
$_['entry_tax_class']  = 'Tipo de Impuesto';
$_['entry_geo_zone']   = 'Zona geográfica';
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Ordenar por';

// Help
$_['help_rate']        = 'Ingresar valores decimales de hasta 5,2. Ejemplo (12345.67):. 1:1,.25:1.27 - peso menor o igual a 0.1 Kg costaría 1,00&euro;, peso inferior o igual a 0,25 g, pero más de 0,1 Kg tendrá un costo de 1,27. No introduzcas KG o símbolos.';

// Error
$_['error_permission'] = 'ADVERTENCIA: ¡No tienes permiso para modificar el envío de Citylink!';