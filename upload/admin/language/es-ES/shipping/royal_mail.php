<?php
// Heading
$_['heading_title']                          = 'Royal Mail';

// Text
$_['text_shipping']                          = 'Envío';
$_['text_success']                           = 'Éxito: Ha modificado Royal Mail Shipping!';
$_['text_edit']                              = 'Editar Royal Mail Shipping';

// Entry
$_['entry_rate']                             = 'Tasas';
$_['entry_insurance']                        = 'Tarifas de compensación';
$_['entry_display_weight']                   = 'Mostrar Peso de Entrega';
$_['entry_display_insurance']                = 'Desplegar seguro';
$_['entry_weight_class']                     = 'Medida de Peso';
$_['entry_tax_class']                        = 'Tipo de Impuesto';
$_['entry_geo_zone']                         = 'Zona geográfica';
$_['entry_status']                           = 'Estado';
$_['entry_sort_order']                       = 'Ordenar por';

// Help
$_['help_rate']                              = 'Ejemplo: 5: 10.00, 7:12.00 Peso: Costo, Peso: Costo, etc...';
$_['help_insurance']                         = 'Ingrese valores arriba de 5,2 lugares decimales. (12345.67) Ejemplo: 34:0,100:1,250:2.25 - Seguro para cubrir valores del carrito hasta 34 costará 0.00 extra, aquellos valores de más de 100 y hasta 250 costará 2.25 extra. No ingrese símbolos de moneda.';
$_['help_airmail_rate_1']                    = 'Ejemplo: 5: 10.00, 7:12.00 Peso: Costo, Peso: Costo, etc...<br /><br />Éstas tarifas sólo se aplicarán a los siguientes países:<br /> AL, AD, AM, AT, AZ, BY, BE, BA, BG, HR, CY, CZ, DK, EE, FO, FI, FR, GE, DE, GI, GR, GL, HU, IS, IE, lo, KZ, KG, LV, LI, LT, LU, MK, MT, MD, MC, NL, NO, PL, PT, RO, RU, SM, SK, SI, ES, SE, CH, TJ, TR, TM, UA, UZ, VA';
$_['help_airmail_rate_2']                    = 'Ejemplo: 5:10. 00, 7:12.00 peso: costo, peso: costo, etc...<br /><br /> éstas tasas sólo se aplicarán a todos los demás países que no están en la lista anterior.';
$_['help_international_signed_rate_1']       = 'Ejemplo: 5: 10.00, 7:12.00 Peso: Costo, Peso: Costo, etc...<br /><br />Éstas tarifas sólo se aplicarán a los siguientes países:<br /> AL, AD, AM, AT, AZ, BY, BE, BA, BG, HR, CY, CZ, DK, EE, FO, FI, FR, GE, DE, GI, GR, GL, HU, IS, IE, lo, KZ, KG, LV, LI, LT, LU, MK, MT, MD, MC, NL, NO, PL, PT, RO, RU, SM, SK, SI, ES, SE, CH, TJ, TR, TM, UA, UZ, VA';
$_['help_international_signed_insurance_1']  = 'Introduzca los valores decimales de hasta 5,2. (12345.67) ejemplo: 34:0, 100: 1, 250:2.25 - El seguro del carro para cubrir valores hasta 34 costaría 0.00 adicionales, para valores de entre 100 y hasta 250 tendrá un costo adicional de 2.25. No ingrese símbolos de moneda. <br /><br /> estas tarifas se aplicarán sólo a los países siguientes: <br /> AL, AD, AM, AT, AZ, BY, BE, BA, BG, HR, CY, CZ, DK, EE, FO, FI, FR, GE, DE, GI, GR, GL, HU, IS, IE, lo, KZ, KG, LV, LI, LT, LU, MK, MT, MD, MC, NL, NO, PL, PT, RO, RU, SM, SK, SI, ES, SE, CH, TJ, TR, TM, UA, UZ, VA';
$_['help_international_signed_rate_2']       = 'Ejemplo: 5: 10.00, 7:12.00 Peso: Costo, Peso: Costo, etc...<br /><br /> Éstas tarifas sólo se aplicarán a todos los demás países que no están en la lista anterior.';
$_['help_international_signed_insurance_2']  = 'Introduzca los valores decimales de hasta 5,2. (12345.67) ejemplo: 34:0, 100: 1, 250:2.25 - El costo del seguro para cubrir valores del carro hasta 34 costaría 0.00 adicionales, valores de más de 100 y hasta 250 tendrá un costo adicional de 2.25. No ingrese símbolos de moneda. <br /><br /> éstas tasas sólo se aplicarán a todos los demás países que no están en la lista anterior.';
$_['help_airsure_rate_1']                    = 'Ejemplo: 5: 10.00, 7:12.00 Peso: Costo, Peso: Costo, etc...<br /><br />Éstas tarifas sólo se aplicarán a los siguientes países:<br />AD, AT, BE, CH, DE, DK, ES, FO, FI, FR, IE, IS, LI, LU, MC, NL, PT, SE';
$_['help_airsure_insurance_1']               = 'Enter values upto 5,2 decimal places. (12345.67) Example: 34:0,100:1,250:2.25 - Insurance cover for cart values upto 34 would cost 0.00 extra, those values more than 100 and upto 250 will cost 2.25 extra. Do not enter currency symbols.<br /><br />These rates will only be applied to the below countries:<br />AD, AT, BE, CH, DE, DK, ES, FO, FI, FR, IE, IS, LI, LU, MC, NL, PT, SE';
$_['help_airsure_rate_2']                    = 'Ejemplo: 5: 10.00, 7:12.00 Peso: Costo, Peso: Costo, etc...<br /><br />Estas tarifas sólo se aplicarán a los siguientes países:<br />BR, CA, HK, MY, NZ, SG, US';
$_['help_airsure_insurance_2']               = 'Introduzca valores decimales de hasta 5,2. (12345.67) ejemplo: 34:0, 100: 1, 250:2.25 - El costo del seguro para cubrir valores del carro hasta 34 costaría 0.00 adicionales, valores de más de 100 y hasta 250 tendrá un costo adicional de 2.25. No ingrese símbolos de moneda. <br /><br />Estas tasas sólo se aplicarán a todos los demás países que no están en la lista anterior:<br />BR, CA, HK, MY, NZ, SG, US';
$_['help_display_weight']                    = '¿Quieres mostrar el peso del envío? (p.ej. peso de entrega: 2,7674 kgs)';
$_['help_display_insurance']                 = '¿Quieres mostrar el seguro de envío? (e.g. asegurado hasta &pound;500)';

// Tab
$_['tab_1st_class_standard']                 = 'Correo estandard primera clase';
$_['tab_1st_class_recorded']                 = 'Correo registrado primera clase';
$_['tab_2nd_class_standard']                 = 'Correo estandard segunda clase';
$_['tab_2nd_class_recorded']                 = 'Correo registrado segunda clase';
$_['tab_special_delivery_500']               = 'Entrega especial día siguiente (&pound;500)';
$_['tab_special_delivery_1000']              = 'Entrega especial día siguiente (&pound;1000)';
$_['tab_special_delivery_2500']              = 'Entrega especial día siguiente (&pound;2500)';
$_['tab_standard_parcels']                   = 'Paquetes estándar (encomiendas)';
$_['tab_airmail']                            = 'Correo aéreo';
$_['tab_international_signed']               = 'Acuse de recibo Internacional';
$_['tab_airsure']                            = 'Airsure';
$_['tab_surface']                            = 'Terrestre';

// Error
$_['error_permission']                       = 'ADVERTENCIA: No tienes permiso para modificar el envío Royal Mail Shipping!';