<?php
// Heading
$_['heading_title']    = 'Envío gratuito';

// Text
$_['text_shipping']    = 'Envío';
$_['text_success']     = 'Éxito: ha modificado el envío gratuito!';
$_['text_edit']        = 'Editar envío gratuito';

// Entry
$_['entry_total']      = 'Total ';
$_['entry_geo_zone']   = 'Zona geográfica';
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Ordenar por';

// Help
$_['help_total']       = 'El importe Subtotal es necesario antes para que el módulo envío gratis esté disponible.';

// Error
$_['error_permission'] = 'ADVERTENCIA: No tienes permiso para modificar envío gratuito!';