<?php
// Heading
$_['heading_title']    = 'Tarifa plana';

// Text
$_['text_shipping']    = 'Envío';
$_['text_success']     = 'Éxito: Ha modificado envío tarifa plana!';
$_['text_edit']        = 'Editar envío de tarifa plana';

// Entry
$_['entry_cost']       = 'Coste';
$_['entry_tax_class']  = 'Tipo de Impuesto';
$_['entry_geo_zone']   = 'Zona geográfica';
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Ordenar por';

// Error
$_['error_permission'] = 'ADVERTENCIA: No tienes permiso para modificar envío tarifa plana!';