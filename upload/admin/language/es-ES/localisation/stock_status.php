<?php
// Heading
$_['heading_title']    = 'Estados de las existencias';

// Text
$_['text_success']     = 'Éxito: Ha modificado los estados de las existencias!';
$_['text_list']        = 'Lista del estado de las existencias';
$_['text_add']         = 'Añadir estado del stock';
$_['text_edit']        = 'Editar estado de las existencias';

// Column
$_['column_name']      = 'Nombre del estado de las existencias';
$_['column_action']    = 'Acción';

// Entry
$_['entry_name']       = 'Nombre del estado de las existencias';

// Error
$_['error_permission'] = 'Atención: No tiene permiso para modificar los estados de las existencias!';
$_['error_name']       = 'El estado de las existencias debe tener entre 3 y 32 caracteres!';
$_['error_product']    = 'ADVERTENCIA: Este estado de las existencias no puede ser eliminada ya que actualmente está asignada a los productos %s!';