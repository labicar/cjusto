<?php
// Heading
$_['heading_title']    = 'Clases de longitud';

// Text
$_['text_success']     = '¡Modificó correctamente las clases de longitud!';
$_['text_list']        = 'Lista de Clases de Longitud';
$_['text_add']         = 'Agregar Clase de Longitud';
$_['text_edit']        = 'Editar Clase de Longitud';

// Column
$_['column_title']     = 'Título de Longitud';
$_['column_unit']      = 'Unidad de Longitud';
$_['column_value']     = 'Valor';
$_['column_action']    = 'Acción';

// Entry
$_['entry_title']      = 'Título de Longitud';
$_['entry_unit']       = 'Unidad de Longitud';
$_['entry_value']      = 'Valor';

// Help
$_['help_value']       = 'Establezca en 1.00000 si este es su longitud por defecto.';

// Error
$_['error_permission'] = '¡Advertencia: No tienes permiso para modificar las clases de longitud!';
$_['error_title']      = '¡Título de la longitud debe ser entre 3 y 32 caracteres!';
$_['error_unit']       = '¡ La Unidad de longitud debe estar entre 1 y 4 caracteres!';
$_['error_default']    = '¡Advertencia: Esta clase de longitud no se puede eliminar actualmente por que está asignada como la clase de longitud predeterminada de la tienda!';
$_['error_product']    = '¡Advertencia: Esta clase de longitud no se puede eliminar actualmente ya que se asigna a los productos %s!';