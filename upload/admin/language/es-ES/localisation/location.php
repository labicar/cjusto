<?php
// Heading
$_['heading_title']    = 'Localización de la Tienda';

// Text
$_['text_success']     = '¡Ha Modificado exitosamente las ubicaciones de almacenamiento!';
$_['text_list']        = 'Lista de ubicación de Tienda';
$_['text_add']         = 'Añadir la ubicación de la Tienda';
$_['text_edit']        = 'Editar Ubicación de la Tienda';
$_['text_default']     = 'Predefinido';
$_['text_time']        = 'Horario de Apertura';
$_['text_geocode']     = 'GeoCoding no tuvo éxito por la siguiente razón:';

// Column
$_['column_name']      = 'Nombre de la Tienda';
$_['column_address']   = 'Dirección';
$_['column_action']    = 'Acción';

// Entry
$_['entry_name']       = 'Nombre de la Tienda';
$_['entry_address']    = 'Dirección';
$_['entry_geocode']    = 'Geocode';
$_['entry_telephone']  = 'Teléfono';
$_['entry_fax']        = 'Fax';
$_['entry_image']      = 'Imagen';
$_['entry_open']       = 'Horario de Apertura';
$_['entry_comment']    = 'Comentario';

// Help
$_['help_geocode']     = 'Introduce manual el GeoCoding de ubicación de la tienda.';
$_['help_open']        = 'Indique su horario de apertura de las tiendas.';
$_['help_comment']     = 'Este campo es para cualquieras notas especiales que te gustaría decirle al cliente, es decir, tienda no acepta cheques.';

// Error
$_['error_permission'] = '¡Advertencia: No tienes permiso para modificar las ubicaciones de la tienda!';
$_['error_name']       = '¡Nombre de la tienda debe tener al menos 1 carácter!';
$_['error_address']    = '¡La dirección debe ser entre 3 y 128 caracteres!';
$_['error_telephone']  = '¡El teléfono debe tener entre 3 y 32 caracteres!';