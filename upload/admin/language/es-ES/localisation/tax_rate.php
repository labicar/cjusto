<?php
// Heading
$_['heading_title']        = 'Tasas de impuesto';

// Text
$_['text_success']         = 'Éxito: Ha modificado las tasas impositivas!';
$_['text_list']            = 'Lista de tasa de impuesto';
$_['text_add']             = 'Agregar tasa impositiva';
$_['text_edit']            = 'Editar tasa impositiva';
$_['text_percent']         = 'Porcentaje';
$_['text_amount']          = 'Cantidad Fija';

// Column
$_['column_name']          = 'Nombre del impuesto';
$_['column_rate']          = 'Tasa de impuesto';
$_['column_type']          = 'Tipo';
$_['column_geo_zone']      = 'Zona geográfica';
$_['column_date_added']    = 'Agregar Fecha';
$_['column_date_modified'] = 'Fecha Modificación';
$_['column_action']        = 'Acción';

// Entry
$_['entry_name']           = 'Nombre del impuesto';
$_['entry_rate']           = 'Tasa de impuesto';
$_['entry_type']           = 'Tipo';
$_['entry_customer_group'] = 'Grupo de Clientes';
$_['entry_geo_zone']       = 'Zona geográfica';

// Error
$_['error_permission']     = 'ADVERTENCIA: No tienes permiso para modificar las tasas impositivas!';
$_['error_tax_rule']       = 'ADVERTENCIA: Este impuesto no se puede eliminar ya que actualmente se asigna a %s clases de impuestos!';
$_['error_name']           = 'Nombre de impuestos debe ser entre 3 y 32 caracteres!';
$_['error_rate']           = 'Tasa de impuesto requerido!';