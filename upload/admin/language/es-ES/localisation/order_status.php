<?php
// Heading
$_['heading_title']    = 'Estados de pedidos';

// Text
$_['text_success']     = '¡Usted ha modificado el estado exitosamente!';
$_['text_list']        = 'Lista de estados';
$_['text_add']         = 'Añadir nuevo estado del pedido';
$_['text_edit']        = 'Editar estado';

// Column
$_['column_name']      = 'Nombre del estado';
$_['column_action']    = 'Acción';

// Entry
$_['entry_name']       = 'Nombre del estado';

// Error
$_['error_permission'] = 'ADVERTENCIA: ¡No tienes permiso para modificar los estados!';
$_['error_name']       = '¡El nombre del estado debe estar entre 3 y 32 caracteres!';
$_['error_default']    = 'ADVERTENCIA: ¡Este estado no puede ser borrado ya que está asignado como el estado por defecto!';
$_['error_download']   = 'ADVERTENCIA: ¡Este estado no puede ser borrado ya que está asignado como el estado de descargas por defecto!';
$_['error_store']      = 'ADVERTENCIA: ¡Este estado no puede ser borrado ya que está asignado a %s tienda(s)!';
$_['error_order']      = 'ADVERTENCIA: ¡Este estado no puede ser borrado ya que está asignado a %s ordenes!';