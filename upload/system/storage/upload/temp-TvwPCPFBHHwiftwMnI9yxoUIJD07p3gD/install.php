<?php
$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."product_to_store_locations` (
  `store_location_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`store_location_id`)
)");

$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."store_to_map` (
  `store_to_map_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_location_id` int(11) NOT NULL,
  `address` text NOT NULL,
  `latitude` varchar(200) NOT NULL,
  `longitude` varchar(200) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`store_to_map_id`)
)");
?>