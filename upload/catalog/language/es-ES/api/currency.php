<?php
// Text
$_['text_success']     = 'Listo: Su moneda ha sido cambiada!';

// Error
$_['error_permission'] = 'ADVERTENCIA: No tienes permiso para acceder a la API!';
$_['error_currency']   = 'Cuidado: El codigo de la moneda es invalido!';